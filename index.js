const Market = require('./lib/market');
const EventBuilder = require('./lib/builders/event-builder');
const InstrumentBuilder = require('./lib/builders/instrument-builder');
const PairItemBuilder = require('./lib/builders/pair-item-builder');
const TokenBuilder = require('./lib/builders/token-builder');
const OrderBuilder = require('./lib/builders/order-builder');
const StopRuleBuilder = require('./lib/builders/stop-rule-builder');
const Utils = require('./lib/utils/base-utils');
class Matchbox {
  static Market(config, eventQueueProviderImpl, instrumentProviderImpl, engineProviderImpl) {
    return Market.create(
      config,
      eventQueueProviderImpl,
      instrumentProviderImpl,
      engineProviderImpl
    );
  }
  static EventBuilder() {
    return EventBuilder.create();
  }
  static InstrumentBuilder() {
    return InstrumentBuilder.create();
  }
  static PairItemBuilder(tokenConfig) {
    return PairItemBuilder.create(tokenConfig);
  }
  static TokenBuilder() {
    return TokenBuilder.create();
  }
  static OrderBuilder() {
    return OrderBuilder.create();
  }
  static StopRuleBuilder() {
    return StopRuleBuilder.create();
  }
  static Utils() {
    return Utils.create();
  }
}

Matchbox.Constants = require('./lib/constants');
Matchbox.QueueBaseProvider = require('./lib/providers/queue/queue-base-provider');
module.exports = Matchbox;
