const QueueFactory = require('./factories/queue-provider-factory');
const InstrumentFactory = require('./factories/instrument-provider-factory');
module.exports = class Market extends require('events').EventEmitter {
  constructor(config, eventQueueProviderImpl, instrumentProviderImpl, engineProviderImpl) {
    super();
    this.config = config || {};
    if (!this.config.name) this.config.name = 'default-matchbox';
    this.utils = require('./utils/base-utils').create();
    this.tokens = [];
    //TODO: make map - instruments can be uniquely identified by their currency pair
    this.instruments = new Map();
    this.instrumentOrders = new Map();
    this.log = this.utils.logger.createLogger({ name: this.config.name });
    this.instrumentProviderImpl =
      instrumentProviderImpl || require('./providers/instrument/instrument-provider');
    this.engineProviderImpl = engineProviderImpl || require('./providers/engine/engine-provider');
    this.eventQueueProviderImpl = eventQueueProviderImpl;
    this.eventQueueProvider = QueueFactory.create(this.config.eventQueue, eventQueueProviderImpl);
  }
  async start() {
    await this.eventQueueProvider.start();
  }
  async stop() {
    await this.eventQueueProvider.stop();
  }
  static create(config, eventQueueProviderImpl, instrumentProviderImpl, engineProviderImpl) {
    return new Market(config, eventQueueProviderImpl, instrumentProviderImpl, engineProviderImpl);
  }
  addToken(token) {
    const found = this.tokens.find(existing => existing.code === token.code);
    if (found != null) this.tokens.splice(this.tokens.indexOf(found), 1);
    this.tokens.push(token);
  }
  getToken(tokenCode, notFoundErrorMessage) {
    const found = this.tokens.find(token => {
      return token.code === tokenCode;
    });
    if (found == null && notFoundErrorMessage) throw new Error(notFoundErrorMessage);
    return found;
  }
  addInstrument(instrumentConfig) {
    const foundBaseToken = this.getToken(
      instrumentConfig.baseToken.tokenCode,
      `could not add instrument, non-existing base token with code [${instrumentConfig.baseToken.tokenCode}]`
    );
    const foundQuoteToken = this.getToken(
      instrumentConfig.quoteToken.tokenCode,
      `could not add instrument, non-existing quote token with code [${instrumentConfig.quoteToken.tokenCode}]`
    );
    instrumentConfig.baseToken.smallestUnitName = foundBaseToken.smallestUnitName;
    instrumentConfig.baseToken.smallestUnitScale = foundBaseToken.smallestUnitScale;
    instrumentConfig.quoteToken.smallestUnitName = foundQuoteToken.smallestUnitName;
    instrumentConfig.quoteToken.smallestUnitScale = foundQuoteToken.smallestUnitScale;
    const pair = `${instrumentConfig.baseToken.tokenCode}/${instrumentConfig.quoteToken.tokenCode}`;
    const found = this.instruments.get(pair);
    if (found != null) throw new Error(`instrument for pair ${pair} already exists`);
    const instrumentInstance = InstrumentFactory.create(
      instrumentConfig,
      this.utils,
      this.eventQueueProvider,
      this.instrumentProviderImpl,
      this.engineProviderImpl,
      this.log
    );
    this.instruments.set(pair, instrumentInstance);
    instrumentInstance.on('order-closed', this.handleOrderClose.bind(this));
    instrumentInstance.on('order-added', this.handleOrderAdd.bind(this));
  }
  handleOrderClose(order) {
    //remove mapping of the order to the instrument
    this.instrumentOrders.delete(order.orderId);
  }
  handleOrderAdd(order) {
    //add mapping of the order to the instrument
    this.instrumentOrders.set(order.orderId, order.instrument);
  }
  getTokens() {
    return this.utils._.cloneDeep(this.tokens);
  }
  getInstrumentConfigs() {
    return this.utils._.cloneDeep(
      Array.from(this.instruments.values()).map(instrument => instrument.config)
    );
  }
  getOrderBook(pair) {
    const instrument = this.getInstrument(
      pair,
      `could not get orderbook for missing instrument: [${pair}]`
    );
    return instrument.getOrderBook();
  }
  async activateInstrument(pair) {
    const instrument = this.getInstrument(pair, true);
    instrument.config.active = true;
  }
  async deactivateInstrument(pair) {
    const instrument = this.__instruments.find(instrument => {
      return instrument.config.pair === pair;
    });
    if (!instrument) throw new Error(`instrument for pair [${pair}] not found`);
    instrument.config.active = false;
  }

  getInstrument(pair, errorMessageIfNotFound) {
    const instrument = this.instruments.get(pair);
    if (instrument == null && errorMessageIfNotFound) throw new Error(errorMessageIfNotFound);
    return instrument;
  }

  getSpread(pair, errorMessageIfNotFound) {
    const instrument = this.instruments.get(pair);
    if (instrument == null && errorMessageIfNotFound) throw new Error(errorMessageIfNotFound);
    return instrument.engine.spread();
  }

  getOrder(orderId, errorMessageIfNotFound) {
    const pair = this.instrumentOrders.get(orderId);
    if (!pair) {
      throw new Error(`no pair matched with ${orderId} found: ${errorMessageIfNotFound}`);
    }
    const instrument = this.getInstrument(pair, `instrument not found for pair ${pair}`);
    const order = instrument.getOrderById(orderId);
    if (order == null && errorMessageIfNotFound) {
      throw new Error(`order with ${orderId} not found: ${errorMessageIfNotFound}`);
    }
    return order;
  }

  getOrdersByTradeAccountId(pair, tradeAccountId) {
    const instrument = this.getInstrument(pair, `instrument not found for pair ${pair}`);
    return instrument.getOrdersByTradeAccountId(tradeAccountId);
  }

  getOrdersBySpotAccountId(pair, spotAccountId) {
    const instrument = this.getInstrument(pair, `instrument not found for pair ${pair}`);
    return instrument.getOrdersBySpotAccountId(spotAccountId);
  }

  orderPush(order) {
    const instrument = this.getInstrument(
      order.instrument,
      `attempt to push order for null instrument [${order.instrument}]`
    );
    if (instrument.config.active !== true)
      throw new Error(`attempt to push order for inactive instrument [${order.instrument}]`);
    return instrument.handleOrderPush(order);
  }
  orderClose(orderId, reason) {
    const order = this.getOrder(orderId, `attempt to close non-active order`);
    const instrument = this.getInstrument(
      order.instrument,
      `attempt to close order for inactive or null instrument [${order.instrument}]`
    );
    return instrument.handleOrderClose(order, reason);
  }
  orderAmend(orderId, changes) {
    const order = this.getOrder(orderId, `attempt to amend non-active order`);
    const instrument = this.getInstrument(
      order.instrument,
      `attempt to amend order for inactive or null instrument [${order.instrument}]`
    );
    return instrument.handleOrderAmend({ order, changes });
  }
};
