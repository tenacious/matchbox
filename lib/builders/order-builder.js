const constants = require(`../constants`);
module.exports = class OrderBuilder extends require('./base-builder') {
  constructor() {
    super();
    this.set('fillOrKill', false, this.TYPES.BOOLEAN);
    this.set('orderType', constants.ORDERS.ORDER_TYPE.MARKET, this.TYPES.INTEGER);
    this.set('timestamp', Date.now());
    this.required([
      'orderId',
      'tradeAccountId',
      'spotAccountId',
      'instrument',
      'direction',
      'quantityRemaining',
      'quantity',
      'timestamp'
    ]);
  }
  static create() {
    return new OrderBuilder();
  }
  withTimestamp(timestamp) {
    return this.set('timestamp', timestamp, this.TYPES.INTEGER);
  }
  withOrderId(orderId) {
    return this.set('orderId', orderId, this.TYPES.STRING);
  }
  withTradeAccountId(tradeAccountId) {
    return this.set('tradeAccountId', tradeAccountId, this.TYPES.STRING);
  }
  withSpotAccountId(spotAccountId) {
    return this.set('spotAccountId', spotAccountId, this.TYPES.STRING);
  }
  withInstrument(instrument) {
    return this.set('instrument', instrument, this.TYPES.STRING);
  }
  withDirection(direction) {
    return this.set('direction', direction, this.TYPES.INTEGER);
  }
  withOrderType(orderType) {
    return this.set('orderType', orderType, this.TYPES.INTEGER);
  }
  withQuantity(quantity) {
    this.set('quantityRemaining', quantity, this.TYPES.NUMERIC);
    return this.set('quantity', quantity, this.TYPES.NUMERIC);
  }
  withPrice(price) {
    return this.set('price', price, this.TYPES.NUMERIC);
  }
  withSystemPrice(systemPrice) {
    return this.set('systemPrice', systemPrice, this.TYPES.NUMERIC);
  }
  withFillOrKill(fillOrKill) {
    return this.set('fillOrKill', fillOrKill, this.TYPES.BOOLEAN);
  }
  withStopRule(stopRule) {
    return this.set('stopRule', stopRule, this.TYPES.OBJECT);
  }
};
