module.exports = class EventBuilder extends require('./base-builder') {
  constructor() {
    super();
    this.required(['eventType', 'payload']);
  }
  static create() {
    return new EventBuilder();
  }
  withEventType(eventType) {
    return this.set('eventType', eventType, this.TYPES.INTEGER);
  }
  withPayload(payload) {
    return this.set('payload', payload, this.TYPES.OBJECT);
  }
  withPair(pair) {
    return this.set('pair', pair, this.TYPES.STRING);
  }
};
