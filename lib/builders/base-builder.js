const mustache = require('mustache');
const utils = require('../utils/base-utils').create();
module.exports = class BaseBuilder {
  constructor(options) {
    this.options = options;
    this.__data = {};
    this.TYPES = {
      STRING: 0,
      NUMERIC: 1,
      OBJECT: 2,
      DATE: 3,
      INTEGER: 4,
      ARRAY: 5
    };
    this.__typeKeys = Object.keys(this.TYPES);
    this.__calculatedFields = {};
    this.utils = utils;
  }
  buildValue(prop) {
    if (prop == null) return prop;
    if (Array.isArray(prop))
      return prop.map(value => {
        return this.buildValue(value);
      });
    return typeof prop.build === 'function' ? prop.build() : prop;
  }
  build() {
    const fieldNames = Object.keys(this.__data);
    const calculatedFieldKeys = Object.keys(this.__calculatedFields);
    if (this.__required) this.checkRequired(fieldNames);
    const result = fieldNames.reduce((json, key) => {
      json[key] = this.buildValue(this.__data[key]);
      return json;
    }, {});
    if (calculatedFieldKeys.length > 0) this.appendCalculatedFields(calculatedFieldKeys, result);
    return result;
  }
  appendCalculatedFields(calculatedFieldKeys, result) {
    return calculatedFieldKeys.reduce((appended, calculatedFieldKey) => {
      if (typeof this.__calculatedFields[calculatedFieldKey] === 'function') {
        appended[calculatedFieldKey] = this.__calculatedFields[calculatedFieldKey].bind(appended)();
      } else
        appended[calculatedFieldKey] = mustache.render(
          this.__calculatedFields[calculatedFieldKey],
          appended
        );
      return appended;
    }, result);
  }
  push(fieldName, value, type, max) {
    if (type != null) this.checkType(value, type, fieldName);
    if (!Array.isArray(this.__data[fieldName])) this.__data[fieldName] = [];
    if (this.__data[fieldName].length === max)
      throw new Error(`maximum allowed items for field[${fieldName}]`);
    this.__data[fieldName].push(value);
    return this;
  }
  set(fieldName, value, type) {
    if (type != null) this.checkType(value, type, fieldName);
    this.__data[fieldName] = value;
    return this;
  }
  setCalculated(fieldName, template) {
    this.__calculatedFields[fieldName] = template;
  }
  required(arrPropertyNames) {
    if (!Array.isArray(arrPropertyNames))
      throw new Error(`argument [arrPropertyNames] at position 0 must be a string array`);
    this.__required = arrPropertyNames;
  }
  checkRequired(fieldNames) {
    for (let fieldName of fieldNames) {
      if (this.__data[fieldName] == null)
        throw new Error(`required field [${fieldName}] cannot be null`);
    }
  }
  checkType(value, type, fieldName) {
    const typeName = this.__typeKeys.find(typeKey => {
      return this.TYPES[typeKey] === type;
    });
    if (!typeName) throw new Error(`unknown type [${type}] specified for field [${fieldName}]`);
    const errMsg = `[${fieldName}] must be of type [${typeName}]`;
    switch (type) {
      case this.TYPES.ARRAY:
        if (!Array.isArray(value)) throw new Error(errMsg);
        break;
      case this.TYPES.OBJECT:
        if (typeof value !== 'object') throw new Error(errMsg);
        break;
      case this.TYPES.STRING:
        if (typeof value !== 'string') throw new Error(errMsg);
        break;
      case this.TYPES.NUMERIC:
        if (typeof this.tryParseFloat(value) !== 'number') throw new Error(errMsg);
        break;
      case this.TYPES.NUMBER:
        if (!Number.isInteger(this.tryParseFloat(value))) throw new Error(errMsg);
        break;
      case this.TYPES.DATE:
        if (!this.tryParseDate(value)) throw new Error(errMsg);
        break;
      default:
        break;
    }
  }
  tryParseFloat(value) {
    try {
      return parseFloat(value);
    } catch (e) {
      return value;
    }
  }
  tryParseDate(value) {
    try {
      return new Date(value);
    } catch (e) {
      return false;
    }
  }
};
