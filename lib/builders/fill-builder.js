const orderCalculations = require('../utils//orders/order-calculations');
module.exports = class FillBuilder extends require('./base-builder') {
  constructor(instrumentConfig) {
    super();
    this.set('timestamp', Date.now());
    this.required([
      'timestamp',
      'bidOrderId',
      'bidQuantity',
      'bidToOffer',
      'bidToFee',
      'offerOrderId',
      'offerQuantity',
      'offerToBid',
      'offerToFee',
      'bidRemainder',
      'offerRemainder',
      'bidPrice',
      'offerPrice',
      'fillPrice'
    ]);
    this.setCalculated('totalBidFilled', function() {
      return orderCalculations
        .totalFilled(
          this.bidToOffer,
          this.bidToFee,
          this.bidRefund,
          instrumentConfig.quoteToken.scale
        )
        .result();
    });
    this.setCalculated('totalOfferFilled', function() {
      return orderCalculations
        .totalFilled(
          this.offerToBid,
          this.offerToFee,
          this.offerRefund,
          instrumentConfig.baseToken.scale
        )
        .result();
    });
  }
  static create(instrumentConfig) {
    return new FillBuilder(instrumentConfig);
  }
  withTimestamp(timestamp) {
    return this.set('timestamp', timestamp, this.TYPES.INTEGER);
  }
  withBidOrderId(bidOrderId) {
    return this.set('bidOrderId', bidOrderId, this.TYPES.STRING);
  }
  withBidQuantity(bidQuantity) {
    return this.set('bidQuantity', bidQuantity, this.TYPES.NUMERIC);
  }
  withBidToOffer(bidToOffer) {
    return this.set('bidToOffer', bidToOffer, this.TYPES.NUMERIC);
  }
  withBidToFee(bidToFee) {
    return this.set('bidToFee', bidToFee, this.TYPES.NUMERIC);
  }
  withBidRefund(bidRefund) {
    return this.set('bidRefund', bidRefund, this.TYPES.NUMERIC);
  }
  withOfferOrderId(offerOrderId) {
    return this.set('offerOrderId', offerOrderId, this.TYPES.STRING);
  }
  withOfferQuantity(offerQuantity) {
    return this.set('offerQuantity', offerQuantity, this.TYPES.NUMERIC);
  }
  withOfferToBid(offerToBid) {
    return this.set('offerToBid', offerToBid, this.TYPES.NUMERIC);
  }
  withOfferToFee(offerToFee) {
    return this.set('offerToFee', offerToFee, this.TYPES.NUMERIC);
  }
  withOfferRefund(offerRefund) {
    return this.set('offerRefund', offerRefund, this.TYPES.NUMERIC);
  }
  withBidRemainder(bidRemainder) {
    return this.set('bidRemainder', bidRemainder, this.TYPES.NUMERIC);
  }
  withOfferRemainder(offerRemainder) {
    return this.set('offerRemainder', offerRemainder, this.TYPES.NUMERIC);
  }
  withBidPrice(bidPrice) {
    return this.set('bidPrice', bidPrice, this.TYPES.NUMERIC);
  }
  withOfferPrice(offerPrice) {
    return this.set('offerPrice', offerPrice, this.TYPES.NUMERIC);
  }
  withFillPrice(fillPrice) {
    return this.set('fillPrice', fillPrice, this.TYPES.NUMERIC);
  }
};
