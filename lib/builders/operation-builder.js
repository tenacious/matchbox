module.exports = class OperationBuilder extends require('./base-builder') {
  constructor() {
    super();
    this.required(['operationType', 'payload']);
  }
  static create() {
    return new OperationBuilder();
  }
  withOperationType(operationType) {
    return this.set('operationType', operationType, this.TYPES.INTEGER);
  }
  withPayload(payload) {
    return this.set('payload', this.utils._.cloneDeep(payload), this.TYPES.OBJECT);
  }
  withPriority(priority) {
    return this.set('priority', priority, this.TYPES.INTEGER);
  }
};
