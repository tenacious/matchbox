module.exports = class InstrumentBuilder extends require('./base-builder') {
  constructor() {
    super();
    this.required(['allowedOrderType', 'baseToken', 'quoteToken']);
    this.setCalculated('pair', '{{baseToken.tokenCode}}/{{quoteToken.tokenCode}}');
  }
  static create() {
    return new InstrumentBuilder();
  }
  withDescription(description) {
    return this.push('description', description, this.TYPES.STRING);
  }
  withAllowedOrderType(orderType) {
    return this.push('allowedOrderType', orderType, this.TYPES.INTEGER);
  }
  withBaseToken(baseToken) {
    return this.set('baseToken', baseToken, this.TYPES.OBJECT);
  }
  withQuoteToken(quoteToken) {
    return this.set('quoteToken', quoteToken, this.TYPES.OBJECT);
  }
};
