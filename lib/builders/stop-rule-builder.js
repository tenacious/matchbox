module.exports = class StopRuleBuilder extends require('./base-builder') {
  constructor() {
    super();
    this.set('fillOrKill', false, this.TYPES.BOOLEAN);
    this.required(['level', 'price']);
  }
  static create() {
    return new StopRuleBuilder();
  }
  withLevel(stopLoss) {
    return this.set('level', stopLoss, this.TYPES.NUMERIC);
  }
  withPrice(price) {
    return this.set('price', price, this.TYPES.NUMERIC);
  }
  withFillOrKill(fillOrKill) {
    return this.set('fillOrKill', fillOrKill, this.TYPES.BOOLEAN);
  }
};
