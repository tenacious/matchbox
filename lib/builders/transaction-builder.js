const orderCalculations = require('../utils//orders/order-calculations');
module.exports = class TransactionBuilder extends require('./base-builder') {
  constructor() {
    super();
    this.required([
      'sourceAccountId',
      'destinationAccountId',
      'amount',
      'operationType',
      'transactionType',
      'tokenCode'
    ]);
    this.setCalculated('totalAmount', function() {
      return orderCalculations.add(this.amount, this.fee).result();
    });
  }
  static create() {
    return new TransactionBuilder();
  }
  withSourceAccountId(sourceAccountId) {
    return this.set('sourceAccountId', sourceAccountId, this.TYPES.STRING);
  }
  withDestinationAccountId(destinationAccountId) {
    return this.set('destinationAccountId', destinationAccountId, this.TYPES.STRING);
  }
  withAmount(amount) {
    return this.set('amount', amount, this.TYPES.NUMERIC);
  }
  withFee(fee) {
    return this.set('fee', fee, this.TYPES.NUMERIC);
  }
  withOperationType(operationType) {
    return this.set('operationType', operationType, this.TYPES.INTEGER);
  }
  withTransactionType(transactionType) {
    return this.set('transactionType', transactionType, this.TYPES.INTEGER);
  }
  withTokenCode(tokenCode) {
    return this.set('tokenCode', tokenCode, this.TYPES.STRING);
  }
  withSourceOrderId(sourceOrderId) {
    return this.set('sourceOrderId', sourceOrderId, this.TYPES.STRING);
  }
  withDestinationOrderId(destinationOrderId) {
    return this.set('destinationOrderId', destinationOrderId, this.TYPES.STRING);
  }
  withOrderPrice(orderPrice) {
    return this.set('orderPrice', orderPrice, this.TYPES.NUMERIC);
  }
  withFillPrice(fillPrice) {
    return this.set('fillPrice', fillPrice, this.TYPES.NUMERIC);
  }
};
