module.exports = class OperationResponseBuilder extends require('./base-builder') {
  constructor() {
    super();
    this.required(['operationStatus']);
  }
  static create() {
    return new OperationResponseBuilder();
  }
  withStatus(operationStatus) {
    return this.set('operationStatus', operationStatus, this.TYPES.INTEGER);
  }
  withResult(result) {
    return this.set('result', this.utils._.cloneDeep(result), this.TYPES.OBJECT);
  }
  withErrorMessage(errorMessage) {
    return this.set('errorMessage', errorMessage, this.TYPES.STRING);
  }
  withErrorStack(errorStack) {
    return this.set('errorStack', errorStack, this.TYPES.STRING);
  }
  withOperationType(operationType) {
    return this.set('operationType', operationType, this.TYPES.INTEGER);
  }
};
