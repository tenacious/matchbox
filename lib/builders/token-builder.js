module.exports = class TokenBuilder extends require('./base-builder') {
  constructor() {
    super();
    this.required(['code', 'smallestUnitName', 'smallestUnitScale']);
  }
  static create() {
    return new TokenBuilder();
  }
  withCode(code) {
    return this.set('code', code, this.TYPES.STRING);
  }
  withDescription(description) {
    return this.set('description', description, this.TYPES.STRING);
  }
  withSmallestUnitName(smallestUnitName) {
    return this.set('smallestUnitName', smallestUnitName, this.TYPES.STRING);
  }
  withSmallestUnitScale(smallestUnitScale) {
    return this.set('smallestUnitScale', smallestUnitScale, this.TYPES.STRING);
  }
};
