const orderCalculations = require('../utils//orders/order-calculations');
module.exports = class PairItemBuilder extends require('./base-builder') {
  constructor(tokenConfig) {
    super();
    this.required(['makerFeePercentage', 'takerFeePercentage']);
    this.withTokenCode(tokenConfig.code);
    this.withSmallestUnitScale(tokenConfig.smallestUnitScale);
    const scale = tokenConfig.smallestUnitScale.length - 1;
    this.withMinFee(orderCalculations.minFee(tokenConfig, scale).result());
    this.withScale(scale);
  }
  static create(tokenConfig) {
    return new PairItemBuilder(tokenConfig);
  }
  withTokenCode(tokenCode) {
    return this.set('tokenCode', tokenCode, this.TYPES.STRING);
  }
  withMinFee(minFee) {
    return this.set('minFee', minFee, this.TYPES.NUMERIC);
  }
  withMakerFeePercentage(makerFeePercentage) {
    return this.set('makerFeePercentage', makerFeePercentage, this.TYPES.NUMERIC);
  }
  withTakerFeePercentage(takerFeePercentage) {
    return this.set('takerFeePercentage', takerFeePercentage, this.TYPES.NUMERIC);
  }
  withSmallestUnitScale(smallestUnitScale) {
    return this.set('smallestUnitScale', smallestUnitScale, this.TYPES.STRING);
  }
  withScale(scale) {
    return this.set('scale', scale, this.TYPES.INTEGER);
  }
};
