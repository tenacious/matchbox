const TransactionBuilder = require('../../builders/transaction-builder');
const EventBuilder = require('../../builders/event-builder');
const EngineFactory = require('../../factories/engine-provider-factory');
const constants = require('../../constants');
const utils = require('../../utils/base-utils').create();
module.exports = class InstrumentProvider extends require('events').EventEmitter {
  constructor(config, utils, eventQueueProvider, engineProviderImpl, logger) {
    super();
    this.config = config;
    this.utils = utils;
    this.logger = logger;
    this.eventQueueProvider = eventQueueProvider;
    this.orderBook = require('./order-book').create(this.config);
    this.engine = EngineFactory.create(this, engineProviderImpl);
  }
  static create(config, utils, eventQueueProvider, engineProviderFactory, logger) {
    return new InstrumentProvider(config, utils, eventQueueProvider, engineProviderFactory, logger);
  }

  getOrderBook() {
    return this.orderBook.generate();
  }

  validateOrder(order) {}

  handleOrderPush(order) {
    order.isTaker = true;
    let matching = true;
    while (matching) {
      matching = this.engine.pull(order);
    }
    return order;
  }

  handleOrderClose(order, reason, quantityRemaining) {
    const activeOrder = this.closeOrder(order, reason, quantityRemaining);
    return [activeOrder, reason];
  }

  closeOrder(order, reason, quantityRemaining) {
    let activeOrder = typeof order === 'string' ? this.getOrderById(order) : order;
    if (!activeOrder.isTaker) {
      this.engine.remove(activeOrder, quantityRemaining);
    }
    if (!utils.orderCalculations.isZero(activeOrder.quantityRemaining)) {
      this.pushTransactionRefund(activeOrder, constants.ENGINE.OPERATION_TYPE.ORDER_CLOSE, reason);
    }
    this.pushEvent(constants.EVENT_TYPE.ORDER_CLOSE, {
      order: activeOrder,
      reason
    });
    this.emit('order-closed', activeOrder);
    return activeOrder;
  }

  getOrderById(orderId) {
    return this.engine.getOrderById(orderId);
  }

  getOrdersByTradeAccountId(tradeAccountId) {
    return this.engine.getOrdersByTradeAccountId(tradeAccountId);
  }

  getOrdersBySpotAccountId(spotAccountId) {
    return this.engine.getOrdersBySpotAccountId(spotAccountId);
  }

  handleOrderAmend(amendment) {
    let activeOrder = this.getOrderById(amendment.order.orderId);
    if (!activeOrder) return; // was filled or cancelled before
    this.engine.remove(activeOrder, activeOrder.quantityRemaining);
    activeOrder.price = amendment.changes.price;
    return this.handleOrderPush(activeOrder);
    //TBD: quantity - which is complex and involves a refund
  }

  handleFill(fill, bidOrder, offerOrder) {
    this.pushTransactionFill(fill, bidOrder, offerOrder);
    this.handleFillOrder(fill, bidOrder);
    this.handleFillOrder(fill, offerOrder);
    this.pushEvent(constants.EVENT_TYPE.FILL, fill);
    return fill;
  }

  handleFillOrder(fill, order) {
    let remainder =
      order.direction === constants.ORDERS.DIRECTION.BID ? fill.bidRemainder : fill.offerRemainder;
    let remainderBeforeFill = order.quantityRemaining;
    order.quantityRemaining = remainder;
    if (utils.orderCalculations.isZero(order.quantityRemaining))
      this.closeOrder(order, constants.ORDERS.CLOSE_TYPE.ORDER_FILLED, remainderBeforeFill);
    else {
      this.updateOrderData(fill, order.direction, order);
    }
  }

  updateOrderData(fill, direction, order) {
    if (!order.isTaker) {
      this.orderBook.remove(
        direction,
        order.orderType,
        order.price,
        direction === constants.ORDERS.DIRECTION.BID ? fill.totalBidFilled : fill.totalOfferFilled
      );
    }
    if (!utils.orderCalculations.isZero(order.quantityRemaining)) {
      this.engine.updateQuantityRemaining(order);
    }
    this.pushEvent(constants.EVENT_TYPE.ORDER_UPDATE, order);
  }

  getTokenCode(order) {
    return order.instrument.split('/')[
      order.direction === constants.ORDERS.DIRECTION.OFFER ? 0 : 1
    ];
  }

  pushTransactionFill(fill, bidOrder, offerOrder) {
    if (!utils.orderCalculations.isZero(fill.bidRefund)) {
      this.pushEvent(
        constants.EVENT_TYPE.TRANSACTION,
        TransactionBuilder.create()
          .withAmount(fill.bidRefund)
          .withSourceAccountId(bidOrder.tradeAccountId)
          .withDestinationAccountId(bidOrder.spotAccountId)
          .withOperationType(constants.ENGINE.OPERATION_TYPE.ORDER_FILL)
          .withTransactionType(constants.ORDERS.CLOSE_TYPE.REFUND)
          .withSourceOrderId(fill.bidOrderId)
          .withTokenCode(this.getTokenCode(bidOrder))
          .withOrderPrice(
            bidOrder.orderType === constants.ORDERS.ORDER_TYPE.MARKET
              ? fill.fillPrice
              : bidOrder.price
          )
          .withFillPrice(fill.fillPrice)
          .build()
      );
    }
    if (!utils.orderCalculations.isZero(fill.offerRefund)) {
      this.pushEvent(
        constants.EVENT_TYPE.TRANSACTION,
        TransactionBuilder.create()
          .withAmount(fill.offerRefund)
          .withSourceAccountId(offerOrder.tradeAccountId)
          .withDestinationAccountId(offerOrder.spotAccountId)
          .withOperationType(constants.ENGINE.OPERATION_TYPE.ORDER_FILL)
          .withTransactionType(constants.ORDERS.CLOSE_TYPE.REFUND)
          .withSourceOrderId(fill.offerOrderId)
          .withTokenCode(this.getTokenCode(offerOrder))
          .withOrderPrice(
            offerOrder.orderType === constants.ORDERS.ORDER_TYPE.MARKET
              ? fill.fillPrice
              : offerOrder.price
          )
          .withFillPrice(fill.fillPrice)
          .build()
      );
    }
    this.pushEvent(
      constants.EVENT_TYPE.TRANSACTION,
      TransactionBuilder.create()
        .withAmount(fill.bidToOffer)
        .withSourceAccountId(bidOrder.tradeAccountId)
        .withDestinationAccountId(offerOrder.spotAccountId)
        .withOperationType(constants.ENGINE.OPERATION_TYPE.ORDER_FILL)
        .withTransactionType(constants.ORDERS.CLOSE_TYPE.ORDER_FILLED)
        .withSourceOrderId(bidOrder.orderId)
        .withDestinationOrderId(offerOrder.orderId)
        .withFee(fill.bidToFee)
        .withTokenCode(this.getTokenCode(bidOrder))
        .withOrderPrice(
          bidOrder.orderType === constants.ORDERS.ORDER_TYPE.MARKET
            ? fill.fillPrice
            : bidOrder.price
        )
        .withFillPrice(fill.fillPrice)
        .build()
    );
    this.pushEvent(
      constants.EVENT_TYPE.TRANSACTION,
      TransactionBuilder.create()
        .withAmount(fill.offerToBid)
        .withSourceAccountId(offerOrder.tradeAccountId)
        .withDestinationAccountId(bidOrder.spotAccountId)
        .withOperationType(constants.ENGINE.OPERATION_TYPE.ORDER_FILL)
        .withTransactionType(constants.ORDERS.CLOSE_TYPE.ORDER_FILLED)
        .withSourceOrderId(offerOrder.orderId)
        .withDestinationOrderId(bidOrder.orderId)
        .withFee(fill.offerToFee)
        .withTokenCode(this.getTokenCode(offerOrder))
        .withOrderPrice(
          offerOrder.orderType === constants.ORDERS.ORDER_TYPE.MARKET
            ? fill.fillPrice
            : offerOrder.price
        )
        .withFillPrice(fill.fillPrice)
        .build()
    );
  }

  pushTransactionRefund(order, operationType, transactionType) {
    this.pushEvent(
      constants.EVENT_TYPE.TRANSACTION,
      TransactionBuilder.create()
        .withAmount(order.quantityRemaining)
        .withSourceAccountId(order.tradeAccountId)
        .withDestinationAccountId(order.spotAccountId)
        .withOperationType(operationType)
        .withTransactionType(transactionType)
        .withSourceOrderId(order.orderId)
        .withTokenCode(this.getTokenCode(order))
        .build()
    );
  }

  pushEvent(eventType, payload) {
    this.eventQueueProvider.push(
      EventBuilder.create()
        .withEventType(eventType)
        .withPayload(payload)
        .withPair(this.config.pair)
    );
  }
};
