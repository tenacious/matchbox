const constants = require('../../constants');

module.exports = class OrderBook {
  constructor(instrumentConfig) {
    this.smallestOfferUnit = instrumentConfig.baseToken.smallestUnitScale;
    this.smallestBidUnit = instrumentConfig.quoteToken.smallestUnitScale;
    this.offerScale = instrumentConfig.baseToken.scale;
    this.bidScale = instrumentConfig.quoteToken.scale;
    this.aggregation = {};
    this.binaryInsert = require('binary-insert').binaryInsert;
    this.orderCalculations = require('../../utils//orders/order-calculations');
  }
  static create(smallestOfferUnit, smallestBidUnit) {
    return new OrderBook(smallestOfferUnit, smallestBidUnit);
  }
  comparator(descending) {
    return descending ? (a, b) => b[0] - a[0] : (a, b) => a[0] - b[0];
  }

  add(direction, type, price, quantity) {
    const orderType = type.toString();
    if (!this.aggregation[orderType])
      this.aggregation[orderType] = {
        [constants.ORDERS.DIRECTION.BID]: {},
        [constants.ORDERS.DIRECTION.OFFER]: {}
      };
    this.aggregation[orderType][direction][price] = this.getAggregationItem(
      direction,
      price,
      quantity,
      this.aggregation[orderType][direction][price]
    );
    return this.aggregation[orderType][direction][price];
  }

  getAggregationItem(direction, price, quantity, previous) {
    let current = [];
    if (previous == null) previous = ['0', '0'];

    if (direction === constants.ORDERS.DIRECTION.BID) {
      current.push(
        this.orderCalculations
          .calculateBaseQuantityFromQuoteQuantity(quantity, price, this.offerScale)
          .result()
      );
      current.push(quantity);
    } else {
      current.push(quantity);
      current.push(
        this.orderCalculations
          .calculateQuoteQuantityFromBaseQuantity(quantity, price, this.bidScale)
          .result()
      );
    }
    return [
      this.orderCalculations.add(previous[0], current[0], this.offerScale).result(),
      this.orderCalculations.add(previous[1], current[1], this.bidScale).result()
    ];
  }
  remove(direction, type, price, quantity) {
    const orderType = type.toString();
    if (!this.aggregation[orderType]) return;
    if (!this.aggregation[orderType][direction]) return;
    if (!this.aggregation[orderType][direction][price]) return;
    let oldQuantity, newQuantity;
    if (direction === constants.ORDERS.DIRECTION.BID) {
      oldQuantity = this.aggregation[orderType][direction][price][1];
      newQuantity = this.orderCalculations.subtract(oldQuantity, quantity, this.bidScale);
    } else {
      oldQuantity = this.aggregation[orderType][direction][price][0];
      newQuantity = this.orderCalculations.subtract(oldQuantity, quantity, this.offerScale);
    }
    if (newQuantity.isZero()) {
      delete this.aggregation[orderType][direction][price];
      return;
    }
    this.aggregation[orderType][direction][price] = this.getAggregationItem(
      direction,
      price,
      newQuantity.result()
    );
  }
  replace(direction, type, price, quantity, newPrice) {
    this.remove(direction, type, price, quantity);
    this.add(direction, type, newPrice, quantity);
  }
  generate() {
    return [
      this.sortedOrders(constants.ORDERS.DIRECTION.OFFER),
      this.sortedOrders(constants.ORDERS.DIRECTION.BID)
    ];
  }
  sortedOrders(direction) {
    const aggregated = [];
    Object.keys(this.aggregation).forEach(orderType => {
      Object.keys(this.aggregation[orderType][direction]).map(price => {
        const orderBookItem = this.aggregation[orderType][direction][price].slice();
        orderBookItem.unshift(price);
        this.binaryInsert(
          aggregated,
          orderBookItem,
          this.comparator(direction === constants.ORDERS.DIRECTION.BID)
        );
      });
    }, []);
    return aggregated;
  }
};
