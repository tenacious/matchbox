module.exports = class OrderCache {
  constructor() {
    this.orders = new Map();
  }
  static create() {
    return new OrderCache();
  }
  get(orderId) {
    return this.orders.get(orderId);
  }
  set(order) {
    this.orders.set(order.orderId, order);
  }
  delete(orderId) {
    this.orders.delete(orderId);
  }
  has(orderId) {
    return this.orders.has(orderId);
  }
  getByTradeAccountId(tradeAccountId) {
    const entries = Array.from(this.orders.values());
    if (tradeAccountId === '*') return entries;
    return entries.filter(order => {
      return order.tradeAccountId === tradeAccountId;
    });
  }
  getBySpotAccountId(spotAccountId) {
    const entries = Array.from(this.orders.values());
    if (spotAccountId === '*') return entries;
    return entries.filter(order => {
      return order.spotAccountId === spotAccountId;
    });
  }
};
