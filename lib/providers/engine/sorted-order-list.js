module.exports = class SortedOrderList {
  constructor(descending, items) {
    this.binaryInsert = require('binary-insert').binaryInsert;
    this.comparator = descending ? (a, b) => b[0] - a[0] : (a, b) => a[0] - b[0];
    this.stack = Array.isArray(items) ? items.sort(this.comparator) : [];
    this.heap = new Map();
    this.index = new Map();
  }

  static create(descending, items) {
    return new SortedOrderList(descending, items);
  }

  insert(item) {
    this.binaryInsert(this.stack, item, this.comparator);
    this.incrementIndex(item);
    this.heap.set(item[2], item);
  }

  incrementIndex(item) {
    let value = this.index.get(item[0]);
    if (value == null) this.index.set(item[0], 0);
    else this.index.set(item[0], (value += 1));
  }

  decrementIndex(item) {
    let value = this.index.get(item[0]);
    if (value != null) {
      this.index.set(item[0], (value -= 1));
      if (value === -1) this.index.delete(item[0]);
    }
  }

  updateQuantity(id, quantity) {
    const item = this.getById(id);
    if (item) {
      item[1] = quantity;
    }
  }

  head() {
    if (this.stack.length === 0) return null;
    return this.stack[this.index.get(this.stack[0][0])];
  }

  getById(id) {
    return this.heap.get(id);
  }

  removeById(id) {
    const item = this.getById(id);
    if (!item) return;
    this.remove(item);
  }

  remove(item) {
    this.removeFromStack(item);
    this.removeFromHeap(item);
  }

  removeFromHeap(item) {
    this.heap.delete(item[2]);
  }

  removeFromStack(item) {
    const index = this.stack.indexOf(item);
    if (index === -1) return;
    this.stack.splice(index, 1);
    this.decrementIndex(item);
  }
};
