const utils = require('../../utils/base-utils').create();
const constants = require('../../constants');
const FillBuilder = require('../../builders/fill-builder');
const orderCalculations = require('../../utils//orders/order-calculations');
module.exports = class Matcher {
  constructor(instrumentConfig, engine) {
    this.instrumentConfig = instrumentConfig;
    this.engine = engine;
  }
  static create(instrumentConfig, engine) {
    return new Matcher(instrumentConfig, engine);
  }

  orderData(infoArray) {
    return this.engine.getOrderById(infoArray[2]);
  }

  getBestByType(orders, orderType) {
    if (orders.type[orderType]) {
      return {
        order: this.orderData(orders.type[orderType]),
        orderType
      };
    }
  }

  getMatch(bestOrders, order) {
    let possibleMatch =
      this.getBestByType(bestOrders, constants.ORDERS.ORDER_TYPE.MID_POINT_PEGGED) ||
      this.getBestByType(bestOrders, constants.ORDERS.ORDER_TYPE.LIMIT);
    let orderData = {
      order,
      orderType: order.orderType
    };
    return {
      bid: order.direction === constants.ORDERS.DIRECTION.BID ? orderData : possibleMatch,
      offer: order.direction === constants.ORDERS.DIRECTION.OFFER ? orderData : possibleMatch
    };
  }

  calculatePrice(matchedPair, direction) {
    let order1, order2;
    if (direction === constants.ORDERS.DIRECTION.BID) {
      order1 = matchedPair.bid;
      order2 = matchedPair.offer;
    } else {
      order1 = matchedPair.offer;
      order2 = matchedPair.bid;
    }
    if (order1.orderType === constants.ORDERS.ORDER_TYPE.MARKET) {
      return order2.order.price;
    }
    return order1.order.price;
  }

  calculateFillPrice(matchedPair, offerPrice, bidPrice) {
    if (matchedPair.bid.orderType === constants.ORDERS.ORDER_TYPE.MARKET) return offerPrice;
    if (matchedPair.offer.orderType === constants.ORDERS.ORDER_TYPE.MARKET) return bidPrice;
    if (matchedPair.bid.order.timestamp > matchedPair.offer.order.timestamp) return bidPrice;
    return offerPrice;
  }

  match(bestOrders, order) {
    const matchedPair = this.getMatch(bestOrders, order);
    const offerPrice = this.calculatePrice(matchedPair, constants.ORDERS.DIRECTION.OFFER);
    const bidPrice = this.calculatePrice(matchedPair, constants.ORDERS.DIRECTION.BID);

    if (orderCalculations.lt(bidPrice, offerPrice)) {
      if (order.fillOrKill) return [false];
      return [null];
    }

    const fillPrice = this.calculateFillPrice(matchedPair, offerPrice, bidPrice);

    const offerBaseQuantity = utils.orderCalculations
      .calculateBaseQuantity(
        constants.ORDERS.DIRECTION.OFFER,
        matchedPair.offer.order.quantityRemaining,
        fillPrice,
        this.instrumentConfig.baseToken.scale
      )
      .result();

    const bidBaseQuantity = utils.orderCalculations
      .calculateBaseQuantity(
        constants.ORDERS.DIRECTION.BID,
        matchedPair.bid.order.quantityRemaining,
        fillPrice,
        this.instrumentConfig.baseToken.scale
      )
      .result();

    const overlap = utils.orderCalculations.calculateOverlap(
      offerBaseQuantity,
      bidBaseQuantity,
      fillPrice,
      this.instrumentConfig.quoteToken.scale
    );

    if (order.fillOrKill) {
      if (
        order.direction === constants.ORDERS.DIRECTION.BID &&
        utils.orderCalculations.lt(overlap.quoteQuantity, order.quantity)
      ) {
        return [false];
      }

      if (
        order.direction === constants.ORDERS.DIRECTION.OFFER &&
        utils.orderCalculations.lt(overlap.baseQuantity, order.quantity)
      ) {
        return [false];
      }
    }

    const offerToFee = utils.orderCalculations
      .calculateFee(
        overlap.baseQuantity,
        this.instrumentConfig.baseToken.makerFeePercentage,
        this.instrumentConfig.baseToken.takerFeePercentage,
        matchedPair.offer.isMaker,
        this.instrumentConfig.baseToken.scale
      )
      .result();

    const bidToFee = utils.orderCalculations
      .calculateFee(
        overlap.quoteQuantity,
        this.instrumentConfig.quoteToken.makerFeePercentage,
        this.instrumentConfig.quoteToken.takerFeePercentage,
        matchedPair.bid.isMaker,
        this.instrumentConfig.quoteToken.scale
      )
      .result();

    const bidToOffer = utils.orderCalculations
      .subtractFee(overlap.quoteQuantity, bidToFee, this.instrumentConfig.quoteToken.scale)
      .result();
    const offerToBid = utils.orderCalculations
      .subtractFee(overlap.baseQuantity, offerToFee, this.instrumentConfig.baseToken.scale)
      .result();

    let bidRemainder = utils.orderCalculations
      .calculateRemaining(
        matchedPair.bid.order.quantityRemaining,
        overlap.quoteQuantity,
        this.instrumentConfig.quoteToken.scale
      )
      .result();

    let offerRemainder = utils.orderCalculations
      .calculateRemaining(
        matchedPair.offer.order.quantityRemaining,
        overlap.baseQuantity,
        this.instrumentConfig.baseToken.scale
      )
      .result();

    const bidRefund = utils.orderCalculations
      .calculateRefund(
        bidRemainder,
        this.instrumentConfig.quoteToken.minFee,
        this.instrumentConfig.quoteToken.makerFeePercentage,
        this.instrumentConfig.quoteToken.takerFeePercentage,
        matchedPair.bid.isMaker,
        this.instrumentConfig.quoteToken.scale
      )
      .result();

    const offerRefund = utils.orderCalculations
      .calculateRefund(
        offerRemainder,
        this.instrumentConfig.baseToken.minFee,
        this.instrumentConfig.baseToken.makerFeePercentage,
        this.instrumentConfig.baseToken.takerFeePercentage,
        matchedPair.offer.isMaker,
        this.instrumentConfig.baseToken.scale
      )
      .result();

    bidRemainder = utils.orderCalculations
      .finalRemainder(bidRemainder, bidRefund, this.instrumentConfig.quoteToken.scale)
      .result();
    offerRemainder = utils.orderCalculations
      .finalRemainder(offerRemainder, offerRefund, this.instrumentConfig.baseToken.scale)
      .result();

    return [
      FillBuilder.create(this.instrumentConfig)
        .withBidOrderId(matchedPair.bid.order.orderId)
        .withBidQuantity(matchedPair.bid.order.quantity)
        .withBidToOffer(bidToOffer)
        .withBidToFee(bidToFee)
        .withOfferOrderId(matchedPair.offer.order.orderId)
        .withOfferQuantity(matchedPair.offer.order.quantity)
        .withOfferToBid(offerToBid)
        .withOfferToFee(offerToFee)
        .withBidRefund(bidRefund)
        .withOfferRefund(offerRefund)
        .withBidRemainder(bidRemainder)
        .withOfferRemainder(offerRemainder)
        .withBidPrice(bidPrice)
        .withOfferPrice(offerPrice)
        .withFillPrice(fillPrice)
        .build(),
      matchedPair.bid.order,
      matchedPair.offer.order
    ];
  }
};
