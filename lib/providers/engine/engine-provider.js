const constants = require('../../constants');
const SortedOrderList = require('./sorted-order-list');
const OrderCache = require('./order-cache');
const Matcher = require('./matcher');
const orderCalculations = require('../../utils//orders/order-calculations');
module.exports = class MatchboxEngine {
  constructor(instrument) {
    this.instrument = instrument;
    this.matcher = Matcher.create(this.instrument.config, this);
    this.orderCache = OrderCache.create();
    this.orderBook = instrument.orderBook;
    this.setupOrderLists();
  }
  static create(instrument) {
    return new MatchboxEngine(instrument);
  }

  async start() {}

  setupOrderLists() {
    this.orderLists = {
      [constants.ORDERS.DIRECTION.BID]: {},
      [constants.ORDERS.DIRECTION.OFFER]: {}
    };
    this.instrument.config.allowedOrderType.forEach(allowedOrderType => {
      this.orderLists[constants.ORDERS.DIRECTION.BID][allowedOrderType] = SortedOrderList.create(
        true
      );
      this.orderLists[constants.ORDERS.DIRECTION.OFFER][
        allowedOrderType
      ] = SortedOrderList.create();
    });
  }

  list(order) {
    const list = this.orderLists[order.direction][order.orderType];
    list.insert([order.price, order.quantityRemaining, order.orderId, order.timestamp]);
    this.orderCache.set(order);
  }

  delist(order) {
    if (!this.orderCache.has(order.orderId)) return null;
    const list = this.orderLists[order.direction][order.orderType];
    list.removeById(order.orderId);
    this.orderCache.delete(order.orderId);
    return order;
  }

  relist(order) {
    this.delist(order);
    this.list(order);
  }

  getOrderById(id) {
    return this.orderCache.has(id) ? this.orderCache.get(id) : null;
  }

  getOrdersByTradeAccountId(tradeAccountId) {
    return this.orderCache.getByTradeAccountId(tradeAccountId);
  }

  getOrdersBySpotAccountId(spotAccountId) {
    return this.orderCache.getBySpotAccountId(spotAccountId);
  }

  spread() {
    let bid = this.orderLists[constants.ORDERS.DIRECTION.OFFER][
      constants.ORDERS.ORDER_TYPE.LIMIT
    ].head();
    let offer = this.orderLists[constants.ORDERS.DIRECTION.BID][
      constants.ORDERS.ORDER_TYPE.LIMIT
    ].head();
    if (!bid || !offer) return [];
    return [offer[0], bid[0]];
  }

  updatePeggedPrices(order, bestOrders) {
    const [lowestOffer, highestBid] = this.spread(order);
    if (lowestOffer == null) return false; //no spread
    const midPoint = orderCalculations.midPoint(lowestOffer, highestBid);
    if (order.orderType === constants.ORDERS.ORDER_TYPE.MID_POINT_PEGGED) {
      order.price = midPoint
        .truncate(
          order.direction === constants.ORDERS.DIRECTION.BID
            ? this.instrument.config.quoteToken.scale
            : this.instrument.config.baseToken.scale
        )
        .result();
    }
    if (bestOrders.type[constants.ORDERS.ORDER_TYPE.MID_POINT_PEGGED] != null) {
      bestOrders.type[constants.ORDERS.ORDER_TYPE.MID_POINT_PEGGED].price = midPoint
        .truncate(
          bestOrders.type[constants.ORDERS.ORDER_TYPE.MID_POINT_PEGGED].direction ===
            constants.ORDERS.DIRECTION.BID
            ? this.instrument.config.quoteToken.scale
            : this.instrument.config.baseToken.scale
        )
        .result();
    }
    return true;
  }

  getBestOrders(order) {
    let pegged = order.orderType === constants.ORDERS.ORDER_TYPE.MID_POINT_PEGGED;
    const bestOrders = this.instrument.config.allowedOrderType.reduce(
      (best, allowedOrderType) => {
        const bestOrder = this.orderLists[
          order.direction === constants.ORDERS.DIRECTION.OFFER
            ? constants.ORDERS.DIRECTION.BID
            : constants.ORDERS.DIRECTION.OFFER
        ][allowedOrderType].head();
        if (bestOrder != null) {
          best.type[allowedOrderType] = bestOrder;
          best.count++;
        }
        if (!pegged && best.orderType === constants.ORDERS.ORDER_TYPE.MID_POINT_PEGGED)
          pegged = true;
        return best;
      },
      { type: {}, count: 0 }
    );
    if (pegged) {
      if (!this.updatePeggedPrices(order, bestOrders)) {
        // no spread - so the order book is empty or single-sided
        return { count: 0 };
      }
    }
    return bestOrders;
  }

  add(order) {
    order.isTaker = false;
    this.list(order);
    this.orderBook.add(order.direction, order.orderType, order.price, order.quantityRemaining);
    this.instrument.emit('order-added', order);
  }

  remove(order, quantityRemaining) {
    const removed = this.delist(order);
    if (removed) {
      this.orderBook.remove(
        order.direction,
        order.orderType,
        order.price,
        quantityRemaining ? quantityRemaining : order.quantityRemaining
      );
    }
  }

  updateQuantityRemaining(order) {
    this.orderLists[order.direction][order.orderType].updateQuantity(
      order.orderId,
      order.quantityRemaining
    );
  }

  pull(order) {
    const bestOrders = this.getBestOrders(order);
    if (bestOrders.count === 0) {
      if (order.orderType === constants.ORDERS.ORDER_TYPE.MARKET) {
        this.instrument.handleOrderClose(order, constants.ORDERS.CLOSE_TYPE.MARKET_EMPTY_REFUND);
      } else {
        this.add(order);
      }
      return false; // nothing waiting, stop pulling
    }
    const [fill, bidOrder, offerOrder] = this.matcher.match(bestOrders, order);
    if (fill == null) {
      return this.add(order); // add to orderbook and stop pulling
    }
    if (fill === false) {
      // fill or kill rule, cancel order
      this.instrument.handleOrderClose(order, constants.ORDERS.CLOSE_TYPE.FILL_OR_KILL_REFUND);
      return false;
    }
    this.instrument.handleFill(fill, bidOrder, offerOrder); //output fills
    return !orderCalculations.amountsEqual(order.quantityRemaining, '0'); //there is more, so keep on pulling
  }
};
