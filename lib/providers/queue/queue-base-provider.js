module.exports = class BaseQueueProvider extends require('events').EventEmitter {
  constructor(config) {
    super();
    this.config = config;
  }
  static create(config) {
    return new BaseQueueProvider(config);
  }
  async start() {
    return await this.startInternal();
  }
  async stop() {
    if (typeof this.startInternal === 'function') return await this.startInternal();
  }
  async push(message) {
    if (typeof message.build === 'function') return await this.pushInternal(message.build());
    return await this.pushInternal(message);
  }
};
