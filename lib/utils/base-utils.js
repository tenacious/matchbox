module.exports = class BaseUtils {
  constructor() {
    this.numbers = require('./numbers/calculation');
    this._ = require('lodash');
    this.logger = require('bunyan');
    this.orderCalculations = require('./orders/order-calculations');
    this.node = require('util'); //attach node utils
  }
  static create() {
    return new BaseUtils();
  }
};
