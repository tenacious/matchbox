const BigNumber = require('big.js');
BigNumber.DP = 12;
BigNumber.RM = BigNumber.roundDown;
module.exports = class Calculation {
  constructor(initialAmount) {
    this.currentAmount = new BigNumber(this.getAmount(initialAmount));
  }

  static chain(initialAmount) {
    return new Calculation(initialAmount);
  }

  getAmount(amount) {
    if (!amount) return 0;
    if (amount.currentAmount) return amount.currentAmount;
    return amount;
  }

  add(amount) {
    this.currentAmount = this.currentAmount.add(this.getAmount(amount));
    return this;
  }

  subtract(amount) {
    this.currentAmount = this.currentAmount.minus(this.getAmount(amount));
    return this;
  }

  divide(amount) {
    this.currentAmount = this.currentAmount.div(this.getAmount(amount));
    return this;
  }

  multiply(amount) {
    this.currentAmount = this.currentAmount.times(this.getAmount(amount));
    return this;
  }

  truncate(to) {
    this.currentAmount = this.currentAmount.round(to);
    return this;
  }

  result(scale) {
    return this.currentAmount.toFixed(scale);
  }

  branch(scale) {
    return new Calculation(this.result(scale));
  }

  isZero() {
    return parseFloat(this.result()) === 0;
  }

  stripTrailingZeroes() {
    return parseFloat(this.result()).toString();
  }

  eq(amount) {
    return this.currentAmount.eq(amount);
  }

  gt(amount) {
    return this.currentAmount.gt(amount);
  }

  lt(amount) {
    return this.currentAmount.lt(amount);
  }

  gte(amount) {
    return this.currentAmount.gte(amount);
  }

  lte(amount) {
    return this.currentAmount.lte(amount);
  }
};
