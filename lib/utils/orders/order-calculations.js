const numbers = require('../numbers/calculation');
const constants = require('../../constants');

module.exports = class OrderCalculations {
  static gt(a, b) {
    return numbers.chain(a).gt(b);
  }

  static lt(a, b) {
    return numbers.chain(a).lt(b);
  }

  static add(a, b, scale) {
    return numbers.chain(a, scale).add(b);
  }

  static subtract(a, b, scale) {
    return numbers.chain(a, scale).subtract(b);
  }

  static midPoint(a, b) {
    return numbers
      .chain(a, 12)
      .add(b)
      .divide('2');
  }

  static minFee(tokenConfig, scale) {
    return numbers
      .chain('1')
      .divide(tokenConfig.smallestUnitScale)
      .truncate(scale);
  }

  static totalFilled(transfer, fee, refund, scale) {
    return numbers
      .chain(transfer)
      .add(fee)
      .add(refund)
      .truncate(scale);
  }

  static finalRemainder(remainder, refund, scale) {
    return numbers
      .chain(remainder)
      .subtract(refund)
      .truncate(scale);
  }

  static isZero(quantity) {
    return numbers.chain(quantity).isZero();
  }

  static amountsEqual(a, b) {
    return numbers.chain(a).eq(b);
  }

  static subtractFee(quantity, fee, scale) {
    return numbers
      .chain(quantity)
      .subtract(fee)
      .truncate(scale);
  }

  static calculateBaseQuantity(orderDirection, quantity, price, scale) {
    if (orderDirection === constants.ORDERS.DIRECTION.OFFER) return numbers.chain(quantity, scale);
    return numbers
      .chain(quantity)
      .divide(price)
      .truncate(scale);
  }

  static calculateQuoteQuantityFromBaseQuantity(baseQuantity, price, scale) {
    return numbers
      .chain(baseQuantity)
      .multiply(price)
      .truncate(scale);
  }

  static calculateBaseQuantityFromQuoteQuantity(quoteQuantity, price, scale) {
    return numbers
      .chain(quoteQuantity)
      .divide(price)
      .truncate(scale);
  }

  static calculateOverlap(offerBaseQuantity, bidBaseQuantity, fillPrice, quoteScale) {
    const baseQuantity = numbers.chain(offerBaseQuantity).lt(bidBaseQuantity)
      ? offerBaseQuantity
      : bidBaseQuantity;
    const quoteQuantity = OrderCalculations.calculateQuoteQuantityFromBaseQuantity(
      baseQuantity,
      fillPrice,
      quoteScale
    );
    return { baseQuantity, quoteQuantity };
  }

  static calculateFee(quantity, makerFeePercentage, takerFeePercentage, isMaker, scale) {
    return numbers
      .chain(quantity)
      .multiply(isMaker ? takerFeePercentage : makerFeePercentage)
      .divide('100')
      .truncate(scale);
  }

  static calculateRemaining(previousQuantity, overlapQuantity, scale) {
    return numbers
      .chain(previousQuantity)
      .subtract(overlapQuantity)
      .truncate(scale);
  }

  static calculateRefund(
    remainderQuantity,
    minFee,
    makerFeePercentage,
    takerFeePercentage,
    isMaker,
    scale
  ) {
    return OrderCalculations.calculateFee(
      remainderQuantity,
      makerFeePercentage,
      takerFeePercentage,
      isMaker,
      scale
    ).lt(minFee)
      ? numbers.chain(remainderQuantity, scale)
      : numbers.chain('0', scale);
  }
};
