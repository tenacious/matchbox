module.exports = {
  REDIS_CLIENT_STATES: {
    UNINITIALIZED: 0,
    READY: 1,
    RECONNECTING: 2,
    ENDED: 3,
    ERROR: 4
  },
  ENTITY_TYPES: {
    INSTRUMENT: 0,
    ORDER: 1,
    FILL: 2,
    TRANSACTION: 3
  },
  NUMBERS: {
    ROUNDING_STRATEGY: {
      ROUND_UP: 0,
      ROUND_DOWN: 1,
      ROUND_CEIL: 2,
      ROUND_FLOOR: 3,
      ROUND_HALF_UP: 4,
      ROUND_HALF_DOWN: 5,
      ROUND_HALF_EVEN: 6,
      ROUND_HALF_CEIL: 7,
      ROUND_HALF_FLOOR: 8
    }
  },
  ORDERS: {
    ORDER_TYPE: {
      MARKET: 0, //take whatever is available, times out after N seconds
      LIMIT: 1, //take the  price or better
      MID_POINT_PEGGED: 2 // put an offer in at the mid-point of the spread
    },
    DIRECTION: {
      BID: 0,
      OFFER: 1
    },
    CLOSE_TYPE: {
      USER_CANCEL: 0,
      SYSTEM_CANCEL: 1,
      REFUND: 2,
      ORDER_FILLED: 3,
      MARKET_EMPTY_REFUND: 4,
      FILL_OR_KILL_REFUND: 5,
      INVALID_ORDER_REFUND: 6
    }
  },
  EVENT_TYPE: {
    ORDER_UPDATE: 0,
    ORDER_CLOSE: 1,
    FILL: 2,
    QUEUE_ERROR: 3,
    OPERATION_ERROR: 4,
    TRANSACTION: 5
  },
  ENGINE: {
    PULL_REASON: {
      ORDER_PUSH: 0,
      ORDER_CLOSE: 1,
      ORDER_AMEND: 2,
      ORDER_FILL: 3,
      PRICE_CHANGE: 4
    },
    OPERATION_STATUS: {
      SUCCESS: 0,
      ERROR: 1
    },
    OPERATION_TYPE: {
      ORDER_PUSH: 0,
      ORDER_CLOSE: 1,
      ORDER_AMEND: 2,
      ORDER_MATCH: 3,
      ORDER_PULL: 4,
      PRICE_CHANGE: 5,
      ORDER_FILL: 6
    },
    OPERATION_PRIORITY: {
      TOP: 0,
      NORMAL: 1
    }
  }
};
