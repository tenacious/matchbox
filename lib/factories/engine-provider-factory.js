module.exports = class EngineProviderFactory {
  static create(instrument, implementation) {
    implementation = implementation || require('../providers/engine/engine-provider');
    return implementation.create(instrument);
  }
};
