module.exports = class InstrumentProviderFactory {
  static create(
    instrumentConfig,
    utils,
    eventQueueProvider,
    instrumentProviderImpl,
    engineProviderImpl,
    logger
  ) {
    const implementation =
      instrumentProviderImpl || require('../providers/instrument/instrument-provider');
    return implementation.create(
      instrumentConfig,
      utils,
      eventQueueProvider,
      engineProviderImpl,
      logger
    );
  }
};
