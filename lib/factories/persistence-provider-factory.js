module.exports = class PersistenceProviderFactory {
  static create(implementation, config, utils) {
    return implementation.create(config, utils);
  }
};
