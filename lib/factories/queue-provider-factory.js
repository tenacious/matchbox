module.exports = class QueueFactory {
  static create(config, implementation) {
    return implementation.create(config);
  }
};
