const test = require('../../../../__fixtures/test-helper').create();
const OrderListBenchmark = require('../../../../__fixtures/helpers/order-list-benchmark-helper');
const INSERTION_COUNT = 50000;
describe(test.name(__filename), function() {
  this.timeout(360000);
  it('inserts a 50000 items in under 1 minutes - desc', async () => {
    OrderListBenchmark.inserts(test, INSERTION_COUNT, 1000, true, 60000, 1000, 50);
  });
  it('inserts a 50000 items in under 1 minutes - asc', async () => {
    OrderListBenchmark.inserts(test, INSERTION_COUNT, 1000, false, 60000, 1000, 50);
  });
});
