const test = require('../../__fixtures/test-helper').create();
const Market = require('../../../lib/market');
const InstrumentBuilder = require('../../../lib/builders/instrument-builder');
const PairItemBuilder = require('../../../lib/builders/pair-item-builder');
const TokenBuilder = require('../../../lib/builders/token-builder');
const OrderBuilder = require('../../../lib/builders/order-builder');
const CONSTANTS = require('../../../lib/constants');
const PERIOD = 5000;
const orderValidator = require('../../__fixtures/validators/order');
const utils = require('../../../lib/utils/base-utils').create();

async function run() {
  const config = {};
  const eventQueueProviderImpl = require('../../__fixtures/providers/event-queue-provider-no-accumulator');

  const market = Market.create(config, eventQueueProviderImpl);
  await setUpMarketScenario1(market);
  await market.start();

  const account1Id = test.uuid.v4();
  const account2Id = test.uuid.v4();
  const accountSpot1Id = test.uuid.v4();
  const accountSpot2Id = test.uuid.v4();

  test.log('building orders...');
  let orders = buildOrders(account1Id, accountSpot1Id, account2Id, accountSpot2Id);
  test.log('orders built...');

  let matching = true;
  let started = Date.now();
  let orderCount = 0;

  setTimeout(async () => {
    matching = false;
    let actualPeriod = Date.now() - started;
    //test.log(market.activePersistenceProvider.data);
    //test.expect(market.activePersistenceProvider.data).to.eql({});
    test.log(market.getOrderBook('BTC/XRP'));
    test.expect(market.getOrderBook('BTC/XRP')).to.eql([[], []]);
    orderValidator.verifyOrderListAgainstOrderBook(test, market); // make  sure there are no leaks of orders
    //test.log(accumulatorIn.results())
    test.log('BENCHMARK SUCCESSFULL');
    test.log('---------------------');
    test.log(
      `outputted ${
        market.eventQueueProvider.queued
      } fill transactions in ${actualPeriod}ms, ${(orderCount / actualPeriod) *
        1000} matches per second`
    );
  }, PERIOD);

  while (matching) {
    market.orderPush(orders.pop());
    orderCount++;
    // await test.delay(100);
    // console.log(`pushed ${orderCount}`);
    if (orderCount % 1000 === 0) {
      test.log(`pushed ${orderCount} orders`);
      // test.log('order book', await market.getOrderBook('BTC/XRP'));
      // test.log('persisted data', market.activePersistenceProvider.data);
      await test.delay(1);
    }
  }
}

function buildOrders(account1Id, accountSpot1Id, account2Id, accountSpot2Id) {
  const orders = [];
  for (var i = 0; i < 100000; i++) {
    let bidQuantity = `${getRandomInt(1, 1000)}.${getRandomInt(1, 1000)}`;
    let price = `${getRandomInt(1, 2)}.${getRandomInt(1, 20)}`;
    let offerQuantity = utils.numbers
      .chain(bidQuantity)
      .divide(price)
      .truncate(8)
      .result();

    orders.push(
      OrderBuilder.create()
        .withOrderId(test.uuid.v4())
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(CONSTANTS.ORDERS.DIRECTION.BID)
        .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity(bidQuantity)
        .withPrice(price)
        .withSpotAccountId(accountSpot1Id)
        .build()
    );

    orders.push(
      OrderBuilder.create()
        .withOrderId(test.uuid.v4())
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(CONSTANTS.ORDERS.DIRECTION.OFFER)
        .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity(offerQuantity)
        .withPrice(price)
        .withSpotAccountId(accountSpot2Id)
        .build()
    );
  }
  return orders;
}

async function setUpMarketScenario1(market) {
  market.addToken(
    TokenBuilder.create()
      .withCode('XRP')
      .withDescription('Ripple token')
      .withSmallestUnitName('drop')
      .withSmallestUnitScale('1000000')
      .build()
  );
  market.addToken(
    TokenBuilder.create()
      .withCode('BTC')
      .withDescription('Bitcoin token')
      .withSmallestUnitName('satoshi')
      .withSmallestUnitScale('100000000')
      .build()
  );
  market.addInstrument(
    InstrumentBuilder.create()
      .withAllowedOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
      .withAllowedOrderType(CONSTANTS.ORDERS.ORDER_TYPE.MARKET)
      .withAllowedOrderType(CONSTANTS.ORDERS.ORDER_TYPE.PEGGED)
      .withBaseToken(
        PairItemBuilder.create(market.getToken('BTC'))
          .withMakerFeePercentage('0.1')
          .withTakerFeePercentage('0.2')
      )
      .withQuoteToken(
        PairItemBuilder.create(market.getToken('XRP'))
          .withMakerFeePercentage('0.1')
          .withTakerFeePercentage('0.2')
      )
      .build()
  );
  await market.activateInstrument('BTC/XRP');
}

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

run();
