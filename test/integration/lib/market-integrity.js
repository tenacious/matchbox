const test = require('../../__fixtures/test-helper').create();
const Market = require('../../../lib/market');
const InstrumentBuilder = require('../../../lib/builders/instrument-builder');
const PairItemBuilder = require('../../../lib/builders/pair-item-builder');
const TokenBuilder = require('../../../lib/builders/token-builder');
const OrderBuilder = require('../../../lib/builders/order-builder');
const CONSTANTS = require('../../../lib/constants');
const PERIOD = 10000;
const orderValidator = require('../../__fixtures/validators/order');
const Accumulator = require('../../__fixtures/helpers/accumulator');
describe(test.name(__filename), function() {
  this.timeout(240000);
  it('matching limit orders', async () => {
    const config = {};
    const utils = require('../../../lib/utils/base-utils').create();
    const eventQueueProviderImpl = require('../../__fixtures/providers/event-queue-provider');

    const market = Market.create(config, eventQueueProviderImpl);
    await setUpMarketScenario1(market);
    await market.start();

    const account1Id = test.uuid.v4();
    const account2Id = test.uuid.v4();
    const accountSpot1Id = test.uuid.v4();
    const accountSpot2Id = test.uuid.v4();

    let matching = true;
    let started = Date.now();
    let accumulatorIn = Accumulator.create();
    let accumulatorOut = market.eventQueueProvider.accumulator;

    setTimeout(async () => {
      matching = false;
      let actualPeriod = Date.now() - started;
      test.expect(market.getOrderBook('BTC/XRP')).to.eql([[], []]);
      await orderValidator.verifyOrderListAgainstOrderBook(test, market); // make  sure there are no leaks of orders
      //test.log(accumulatorIn.results());
      test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
      test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
      test.expect(accumulatorIn.results()).to.eql(accumulatorOut.results());
      test.log('INTEGRITY CHECK SUCCESSFULL');
      test.log('---------------------');
      test.log(
        `outputted ${
          market.eventQueueProvider.accumulated[3]
        } fill transactions in ${actualPeriod}ms, ${market.eventQueueProvider.accumulated[3] /
          2 /
          (actualPeriod / 1000)} matches per second`
      );
      await market.stop();
    }, PERIOD);

    let orderCount = 0;
    while (matching) {
      let bidQuantity = `${getRandomInt(1, 1000)}.${getRandomInt(1, 1000)}`;
      let price = `${getRandomInt(1, 2)}.${getRandomInt(1, 20)}`;
      let offerQuantity = utils.numbers
        .chain(bidQuantity)
        .divide(price)
        .truncate(8)
        .result();

      accumulatorIn.add('XRP', bidQuantity);
      accumulatorIn.add('BTC', offerQuantity);

      market.orderPush(
        OrderBuilder.create()
          .withOrderId(test.uuid.v4())
          .withTradeAccountId(account1Id)
          .withInstrument('BTC/XRP')
          .withDirection(CONSTANTS.ORDERS.DIRECTION.BID)
          .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
          .withQuantity(bidQuantity)
          .withPrice(price)
          .withSpotAccountId(accountSpot1Id)
          .build()
      );
      market.orderPush(
        OrderBuilder.create()
          .withOrderId(test.uuid.v4())
          .withTradeAccountId(account2Id)
          .withInstrument('BTC/XRP')
          .withDirection(CONSTANTS.ORDERS.DIRECTION.OFFER)
          .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
          .withQuantity(offerQuantity)
          .withPrice(price)
          .withSpotAccountId(accountSpot2Id)
          .build()
      );
      orderCount++;
      if (orderCount % 1000 === 0) {
        test.log(`pushed ${orderCount} orders`);
        // test.log('order book', market.getOrderBook('BTC/XRP'));
        // test.log('persisted data', market.activePersistenceProvider.data);
        await test.delay(1);
      }
    }
  });

  it('partial fills - limit orders', async () => {
    const config = {};
    const utils = require('../../../lib/utils/base-utils').create();
    const eventQueueProviderImpl = require('../../__fixtures/providers/event-queue-provider');

    const market = Market.create(config, eventQueueProviderImpl);
    await setUpMarketScenario1(market);
    await market.start();

    const account1Id = test.uuid.v4();
    const account2Id = test.uuid.v4();
    const accountSpot1Id = test.uuid.v4();
    const accountSpot2Id = test.uuid.v4();

    let accumulatorIn = Accumulator.create();
    let accumulatorOut = market.eventQueueProvider.accumulator;

    let bidQuantity = `1000`;
    let price = `2`;
    let offerQuantity = utils.numbers
      .chain(bidQuantity)
      .divide(price)
      .truncate(8)
      .result();

    let offerQuantity1 = utils.numbers
      .chain(offerQuantity)
      .divide('2')
      .truncate(8)
      .result();

    let offerQuantity2 = utils.numbers
      .chain(offerQuantity)
      .subtract(offerQuantity1)
      .truncate(8)
      .result();

    accumulatorIn.add('XRP', bidQuantity);
    accumulatorIn.add('BTC', offerQuantity);

    market.orderPush(
      OrderBuilder.create()
        .withOrderId(test.uuid.v4())
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(CONSTANTS.ORDERS.DIRECTION.BID)
        .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity(bidQuantity)
        .withPrice(price)
        .withSpotAccountId(accountSpot1Id)
        .build()
    );
    market.orderPush(
      OrderBuilder.create()
        .withOrderId(test.uuid.v4())
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(CONSTANTS.ORDERS.DIRECTION.OFFER)
        .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity(offerQuantity1)
        .withPrice(price)
        .withSpotAccountId(accountSpot2Id)
        .build()
    );
    market.orderPush(
      OrderBuilder.create()
        .withOrderId(test.uuid.v4())
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(CONSTANTS.ORDERS.DIRECTION.OFFER)
        .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity(offerQuantity2)
        .withPrice(price)
        .withSpotAccountId(accountSpot2Id)
        .build()
    );
    //test.log(market.activePersistenceProvider.data);
    test.expect(market.getOrderBook('BTC/XRP')).to.eql([[], []]);
    await orderValidator.verifyOrderListAgainstOrderBook(test, market); // make  sure there are no leaks of orders
    //test.log(accumulatorIn.results());
    test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
    test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
    test.expect(accumulatorIn.results()).to.eql(accumulatorOut.results());
    test.log('INTEGRITY CHECK SUCCESSFULL');
    test.log('---------------------');
    await market.stop();
  });

  it('partial fills - limit orders - multiple', async () => {
    const config = {};
    const utils = require('../../../lib/utils/base-utils').create();
    const eventQueueProviderImpl = require('../../__fixtures/providers/event-queue-provider');

    const market = Market.create(config, eventQueueProviderImpl);
    await setUpMarketScenario1(market);
    await market.start();

    const account1Id = test.uuid.v4();
    const account2Id = test.uuid.v4();
    const accountSpot1Id = test.uuid.v4();
    const accountSpot2Id = test.uuid.v4();

    let matching = true;
    let started = Date.now();
    let accumulatorIn = Accumulator.create();
    let accumulatorOut = market.eventQueueProvider.accumulator;

    setTimeout(async () => {
      matching = false;
      let actualPeriod = Date.now() - started;
      test.expect(market.getOrderBook('BTC/XRP')).to.eql([[], []]);
      await orderValidator.verifyOrderListAgainstOrderBook(test, market); // make  sure there are no leaks of orders
      test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
      test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
      test.expect(accumulatorIn.results()).to.eql(accumulatorOut.results());
      test.log('INTEGRITY CHECK SUCCESSFUL');
      test.log('---------------------');
      test.log(
        `outputted ${
          market.eventQueueProvider.accumulated[3]
        } fill transactions in ${actualPeriod}ms, ${market.eventQueueProvider.accumulated[3] /
          2 /
          (actualPeriod / 1000)} matches per second`
      );
      await market.stop();
    }, PERIOD);

    let orderCount = 0;
    while (matching) {
      let bidQuantity = `${getRandomInt(1, 1000)}.${getRandomInt(1, 1000)}`;
      let price = `${getRandomInt(1, 2)}.${getRandomInt(1, 20)}`;
      let offerQuantity = utils.numbers
        .chain(bidQuantity)
        .divide(price)
        .truncate(8)
        .result();

      let offerQuantity1 = utils.numbers
        .chain(offerQuantity)
        .divide('2')
        .truncate(8)
        .result();

      let offerQuantity2 = utils.numbers
        .chain(offerQuantity)
        .subtract(offerQuantity1)
        .truncate(8)
        .result();

      accumulatorIn.add('XRP', bidQuantity);
      accumulatorIn.add('BTC', offerQuantity);

      market.orderPush(
        OrderBuilder.create()
          .withOrderId(test.uuid.v4())
          .withTradeAccountId(account1Id)
          .withInstrument('BTC/XRP')
          .withDirection(CONSTANTS.ORDERS.DIRECTION.BID)
          .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
          .withQuantity(bidQuantity)
          .withPrice(price)
          .withSpotAccountId(accountSpot1Id)
          .build()
      );
      market.orderPush(
        OrderBuilder.create()
          .withOrderId(test.uuid.v4())
          .withTradeAccountId(account2Id)
          .withInstrument('BTC/XRP')
          .withDirection(CONSTANTS.ORDERS.DIRECTION.OFFER)
          .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
          .withQuantity(offerQuantity1)
          .withPrice(price)
          .withSpotAccountId(accountSpot2Id)
          .build()
      );
      market.orderPush(
        OrderBuilder.create()
          .withOrderId(test.uuid.v4())
          .withTradeAccountId(account2Id)
          .withInstrument('BTC/XRP')
          .withDirection(CONSTANTS.ORDERS.DIRECTION.OFFER)
          .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
          .withQuantity(offerQuantity2)
          .withPrice(price)
          .withSpotAccountId(accountSpot2Id)
          .build()
      );
      orderCount++;
      if (orderCount % 1000 === 0) {
        test.log(`pushed ${orderCount} orders`);
        // test.log('order book', market.getOrderBook('BTC/XRP'));
        // test.log('persisted data', market.activePersistenceProvider.data);
        await test.delay(1);
      }
    }
  });

  it('market orders closing - market bid', async () => {
    const config = {};
    const utils = require('../../../lib/utils/base-utils').create();
    const eventQueueProviderImpl = require('../../__fixtures/providers/event-queue-provider');

    const market = Market.create(config, eventQueueProviderImpl);
    await setUpMarketScenario1(market);
    await market.start();

    const account1Id = test.uuid.v4();
    const account2Id = test.uuid.v4();
    const accountSpot1Id = test.uuid.v4();
    const accountSpot2Id = test.uuid.v4();

    let accumulatorIn = Accumulator.create();
    let accumulatorOut = market.eventQueueProvider.accumulator;

    let bidQuantity = `1000`;
    let price = `2`;
    let offerQuantity = utils.numbers
      .chain(bidQuantity)
      .divide(price)
      .truncate(8)
      .result();

    accumulatorIn.add('XRP', bidQuantity);
    accumulatorIn.add('BTC', offerQuantity);

    market.orderPush(
      OrderBuilder.create()
        .withOrderId(test.uuid.v4())
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(CONSTANTS.ORDERS.DIRECTION.OFFER)
        .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity(offerQuantity)
        .withPrice(price)
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    market.orderPush(
      OrderBuilder.create()
        .withOrderId(test.uuid.v4())
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(CONSTANTS.ORDERS.DIRECTION.BID)
        .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.MARKET)
        .withQuantity(bidQuantity)
        .withSpotAccountId(accountSpot1Id)
        .build()
    );
    //test.log(market.activePersistenceProvider.data);
    test.expect(market.getOrderBook('BTC/XRP')).to.eql([[], []]);
    await orderValidator.verifyOrderListAgainstOrderBook(test, market); // make  sure there are no leaks of orders
    //test.log(accumulatorIn.results());
    test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
    test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
    test.expect(accumulatorIn.results()).to.eql(accumulatorOut.results());
    test.log('INTEGRITY CHECK SUCCESSFUL');
    test.log('---------------------');
    await market.stop();
  });

  it('market orders closing - market bid refund', async () => {
    const config = {};
    const utils = require('../../../lib/utils/base-utils').create();
    const eventQueueProviderImpl = require('../../__fixtures/providers/event-queue-provider');

    const market = Market.create(config, eventQueueProviderImpl);
    await setUpMarketScenario1(market);
    await market.start();

    const account1Id = test.uuid.v4();
    const account2Id = test.uuid.v4();
    const accountSpot1Id = test.uuid.v4();
    const accountSpot2Id = test.uuid.v4();

    let accumulatorIn = Accumulator.create();
    let accumulatorOut = market.eventQueueProvider.accumulator;

    let bidQuantity = `1000`;
    let price = `2`;
    let offerQuantity = utils.numbers
      .chain(bidQuantity)
      .divide(price)
      .truncate(8)
      .result();

    accumulatorIn.add('XRP', '1500');
    accumulatorIn.add('BTC', offerQuantity);

    market.orderPush(
      OrderBuilder.create()
        .withOrderId(test.uuid.v4())
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(CONSTANTS.ORDERS.DIRECTION.OFFER)
        .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity(offerQuantity)
        .withPrice(price)
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    market.orderPush(
      OrderBuilder.create()
        .withOrderId(test.uuid.v4())
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(CONSTANTS.ORDERS.DIRECTION.BID)
        .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.MARKET)
        .withQuantity('1500')
        .withSpotAccountId(accountSpot1Id)
        .build()
    );
    test.expect(market.getOrderBook('BTC/XRP')).to.eql([[], []]);
    await orderValidator.verifyOrderListAgainstOrderBook(test, market); // make  sure there are no leaks of orders
    test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
    test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
    test.expect(accumulatorIn.results()).to.eql(accumulatorOut.results());
    test.log('INTEGRITY CHECK SUCCESSFUL');
    test.log('---------------------');
    await market.stop();
  });

  it('market orders closing - market offer refund', async () => {
    const config = {};
    const utils = require('../../../lib/utils/base-utils').create();
    const eventQueueProviderImpl = require('../../__fixtures/providers/event-queue-provider');

    const market = Market.create(config, eventQueueProviderImpl);
    await setUpMarketScenario1(market);
    await market.start();

    const account1Id = test.uuid.v4();
    const account2Id = test.uuid.v4();
    const accountSpot1Id = test.uuid.v4();
    const accountSpot2Id = test.uuid.v4();

    let accumulatorIn = Accumulator.create();
    let accumulatorOut = market.eventQueueProvider.accumulator;

    let bidQuantity = `1000`;
    let price = `1`;
    let offerQuantity = utils.numbers
      .chain(bidQuantity)
      .multiply(2)
      .truncate(8)
      .result();

    accumulatorIn.add('XRP', bidQuantity);
    accumulatorIn.add('BTC', '2000');

    market.orderPush(
      OrderBuilder.create()
        .withOrderId(test.uuid.v4())
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(CONSTANTS.ORDERS.DIRECTION.BID)
        .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity(bidQuantity)
        .withPrice(price)
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    market.orderPush(
      OrderBuilder.create()
        .withOrderId(test.uuid.v4())
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(CONSTANTS.ORDERS.DIRECTION.OFFER)
        .withOrderType(CONSTANTS.ORDERS.ORDER_TYPE.MARKET)
        .withQuantity(offerQuantity)
        .withSpotAccountId(accountSpot1Id)
        .build()
    );
    //test.log(market.activePersistenceProvider.data);
    test.expect(market.getOrderBook('BTC/XRP')).to.eql([[], []]);
    await orderValidator.verifyOrderListAgainstOrderBook(test, market); // make  sure there are no leaks of orders
    //test.log(accumulatorIn.results());
    test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
    test.assert(parseFloat(accumulatorIn.results()[0].total) > 0);
    test.expect(accumulatorIn.results()).to.eql(accumulatorOut.results());
    test.log('INTEGRITY CHECK SUCCESSFUL');
    test.log('---------------------');
    await market.stop();
  });

  xit('combined order types', async () => {
    //TODO: not implemented yet
  });

  async function setUpMarketScenario1(market) {
    market.addToken(
      TokenBuilder.create()
        .withCode('XRP')
        .withDescription('Ripple token')
        .withSmallestUnitName('drop')
        .withSmallestUnitScale('1000000')
        .build()
    );
    market.addToken(
      TokenBuilder.create()
        .withCode('BTC')
        .withDescription('Bitcoin token')
        .withSmallestUnitName('satoshi')
        .withSmallestUnitScale('100000000')
        .build()
    );
    market.addInstrument(
      InstrumentBuilder.create()
        .withAllowedOrderType(CONSTANTS.ORDERS.ORDER_TYPE.LIMIT)
        .withAllowedOrderType(CONSTANTS.ORDERS.ORDER_TYPE.MARKET)
        .withAllowedOrderType(CONSTANTS.ORDERS.ORDER_TYPE.MID_POINT_PEGGED)
        .withBaseToken(
          PairItemBuilder.create(market.getToken('BTC'))
            .withMakerFeePercentage('0.1')
            .withTakerFeePercentage('0.2')
        )
        .withQuoteToken(
          PairItemBuilder.create(market.getToken('XRP'))
            .withMakerFeePercentage('0.1')
            .withTakerFeePercentage('0.2')
        )
        .build()
    );
    market.activateInstrument('BTC/XRP');
  }

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
});
