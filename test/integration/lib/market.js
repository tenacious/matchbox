const test = require('../../__fixtures/test-helper').create();
const Matchbox = require('matchbox-js');
const constants = Matchbox.Constants;
const orderValidator = require('../../__fixtures/validators/order');
const mockData = require('../../__fixtures/data/integration-lib-market').create();

describe(test.name(__filename), function() {
  this.timeout(240000);
  it('sets up a market', async () => {
    const config = {};
    //test implementations
    const eventQueueProviderImpl = require('../../__fixtures/providers/event-queue-provider');
    const market = Matchbox.Market(config, eventQueueProviderImpl);
    await setUpMarketScenario1(market);
  });
  it('limit orders with cancellations, fills and refunds', async () => {
    const config = {};
    const eventQueueProviderImpl = require('../../__fixtures/providers/event-queue-provider');

    const market = Matchbox.Market(config, eventQueueProviderImpl);
    await setUpMarketScenario1(market);
    await market.start();

    const account1Id = test.uuid.v4();
    const account2Id = test.uuid.v4();
    const accountSpot1Id = test.uuid.v4();
    const accountSpot2Id = test.uuid.v4();

    const order1Id = test.uuid.v4();
    const order2Id = test.uuid.v4();
    const order3Id = test.uuid.v4();
    const order4Id = test.uuid.v4();
    const order5Id = test.uuid.v4();
    const order6Id = test.uuid.v4();
    const order7Id = test.uuid.v4();
    const order8Id = test.uuid.v4();
    const order9Id = test.uuid.v4();
    const order10Id = test.uuid.v4();

    const result1 = market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order1Id)
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.BID)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity('10')
        .withPrice('2')
        .withFillOrKill(false) //wont go into the orderbook if true
        .withSpotAccountId(accountSpot1Id)
        .build()
    );
    const result2 = market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order2Id)
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.OFFER)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity('5')
        .withPrice('2.5')
        .withFillOrKill(false) //if this order is not filled in one go, close it and emit refund transaction, wont go into the orderbook if true
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    verifyOperations1(
      result1,
      result2,
      order1Id,
      account1Id,
      accountSpot1Id,
      order2Id,
      account2Id,
      accountSpot2Id
    );

    const orderBook = market.getOrderBook('BTC/XRP');

    test.expect(orderBook).to.eql([[['2.5', '5', '12.5']], [['2', '5', '10']]]);

    test.expect(market.getSpread('BTC/XRP')).to.eql(['2', '2.5']);

    market.orderAmend(order1Id, { price: '2.3' });

    test
      .expect(market.getOrderBook('BTC/XRP'))
      .to.eql([[['2.5', '5', '12.5']], [['2.3', '4.34782608', '10']]]);

    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order3Id)
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.OFFER)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity('10')
        .withPrice('2.4')
        .withFillOrKill(false) //if this order is not filled in one go, close it and emit refund transaction, wont go into the orderbook if true
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order4Id)
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.BID)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity('10')
        .withPrice('2.2')
        .withFillOrKill(false) //if this order is not filled in one go, close it and emit refund transaction, wont go into the orderbook if true
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    test.expect(market.getOrderBook('BTC/XRP')).to.eql([
      [
        ['2.4', '10', '24'],
        ['2.5', '5', '12.5']
      ],
      [
        ['2.3', '4.34782608', '10'],
        ['2.2', '4.54545454', '10']
      ]
    ]);
    orderValidator.verifyOrderListAgainstOrderBook(test, market);

    market.orderClose(order3Id, constants.ORDERS.CLOSE_TYPE.USER_CANCEL);

    test.expect(market.getOrderBook('BTC/XRP')).to.eql([
      [['2.5', '5', '12.5']],
      [
        ['2.3', '4.34782608', '10'],
        ['2.2', '4.54545454', '10']
      ]
    ]);
    orderValidator.verifyOrderListAgainstOrderBook(test, market);

    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order5Id)
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.BID)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity('10')
        .withPrice('2.2')
        .withFillOrKill(false) //if this order is not filled in one go, close it and emit refund transaction, wont go into the orderbook if true
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    test.expect(market.getOrderBook('BTC/XRP')).to.eql([
      [['2.5', '5', '12.5']],
      [
        ['2.3', '4.34782608', '10'],
        ['2.2', '9.09090908', '20']
      ]
    ]);
    orderValidator.verifyOrderListAgainstOrderBook(test, market);

    market.orderClose(order5Id, constants.ORDERS.CLOSE_TYPE.USER_CANCEL);

    test.expect(market.getOrderBook('BTC/XRP')).to.eql([
      [['2.5', '5', '12.5']],
      [
        ['2.3', '4.34782608', '10'],
        ['2.2', '4.54545454', '10']
      ]
    ]);
    orderValidator.verifyOrderListAgainstOrderBook(test, market);
    //check the  cancellations went to the transactionsQueue
    test
      .expect(mockData.getExpectedQueueItems1(account2Id, accountSpot2Id, order3Id, order5Id))
      .to.eql(market.eventQueueProvider.queued.filter(item => item.payload.transactionType === 0));

    //amend an offer up - no fills will happen
    market.orderAmend(order2Id, { price: '2.6' });
    test.expect(market.getOrderBook('BTC/XRP')).to.eql([
      [['2.6', '5', '13']],
      [
        ['2.3', '4.34782608', '10'],
        ['2.2', '4.54545454', '10']
      ]
    ]);

    //amend an offer down - fills will now kick in
    market.orderAmend(order2Id, { price: '2.3' });

    let expectedQueueItems2 = mockData.getExpectedQueueItems2(
      account1Id,
      accountSpot1Id,
      account2Id,
      accountSpot2Id,
      order1Id,
      order2Id,
      order3Id,
      order5Id
    );

    //check our latest fills are in the transaction queue
    test
      .expect(expectedQueueItems2)
      .to.eql(market.eventQueueProvider.queued.filter(item => item.eventType === 5));

    test
      .expect(
        test.utils.numbers
          .chain(expectedQueueItems2[2].payload.amount) //discount
          .add(expectedQueueItems2[3].payload.amount) //bid to offer
          .add(expectedQueueItems2[3].payload.fee) // bid to fee
          .stripTrailingZeroes()
      )
      .to.eql('10');

    test
      .expect(market.getOrderBook('BTC/XRP'))
      .to.eql([[['2.3', '0.65217392', '1.5']], [['2.2', '4.54545454', '10']]]);
    orderValidator.verifyOrderListAgainstOrderBook(test, market);

    //amend an offer down - fills will now kick in
    market.orderAmend(order2Id, { price: '2.2' });

    test.expect(market.getOrderBook('BTC/XRP')).to.eql([[], [['2.2', '3.8932809', '8.565218']]]);
    orderValidator.verifyOrderListAgainstOrderBook(test, market);

    let expectedQueueItems3 = mockData
      .getExpectedQueueItems3(
        account1Id,
        accountSpot1Id,
        account2Id,
        accountSpot2Id,
        order1Id,
        order2Id,
        order3Id,
        order4Id,
        order5Id
      )
      .map(removeTimestamps);

    test.expect(expectedQueueItems3).to.eql(market.eventQueueProvider.queued.map(removeTimestamps));

    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order6Id)
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.OFFER)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withQuantity('10')
        .withPrice('2.5')
        .withFillOrKill(false) //if this order is not filled in one go, close it and emit refund transaction, wont go into the orderbook if true
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    const orderBookSaved = JSON.stringify(market.getOrderBook('BTC/XRP'), null, 2);

    // test fill or kill - non matching prices
    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order7Id)
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.OFFER)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withPrice('2.6')
        .withQuantity('1000000')
        .withFillOrKill(true) //if this order is not filled in one go, close it and emit refund transaction, wont go into the orderbook if true
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    test.expect(orderBookSaved).to.eql(JSON.stringify(market.getOrderBook('BTC/XRP'), null, 2));
    let [secondLastTransaction, lastTransaction] = market.eventQueueProvider.queued.slice(
      market.eventQueueProvider.queued.length - 2
    );
    test.expect(lastTransaction.eventType).to.equal(constants.EVENT_TYPE.ORDER_CLOSE);
    test
      .expect(lastTransaction.payload.reason)
      .to.equal(constants.ORDERS.CLOSE_TYPE.FILL_OR_KILL_REFUND);
    test.expect(lastTransaction.payload.order.orderId).to.equal(order7Id);
    test.expect(secondLastTransaction.eventType).to.equal(constants.EVENT_TYPE.TRANSACTION);
    test.expect(secondLastTransaction.payload.totalAmount).to.equal('1000000');
    test
      .expect(secondLastTransaction.payload.transactionType)
      .to.equal(constants.ORDERS.CLOSE_TYPE.FILL_OR_KILL_REFUND);

    // test fill or kill - matching prices - offer in
    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order8Id)
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.OFFER)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withPrice('2.2')
        .withQuantity('1000001')
        .withFillOrKill(true) //if this order is not filled in one go, close it and emit refund transaction, wont go into the orderbook if true
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    test.expect(orderBookSaved).to.eql(JSON.stringify(market.getOrderBook('BTC/XRP'), null, 2));

    [secondLastTransaction, lastTransaction] = market.eventQueueProvider.queued.slice(
      market.eventQueueProvider.queued.length - 2
    );

    test.expect(lastTransaction.eventType).to.equal(constants.EVENT_TYPE.ORDER_CLOSE);
    test
      .expect(lastTransaction.payload.reason)
      .to.equal(constants.ORDERS.CLOSE_TYPE.FILL_OR_KILL_REFUND);
    test.expect(lastTransaction.payload.order.orderId).to.equal(order8Id);
    test.expect(secondLastTransaction.eventType).to.equal(constants.EVENT_TYPE.TRANSACTION);
    test.expect(secondLastTransaction.payload.totalAmount).to.equal('1000001');
    test
      .expect(secondLastTransaction.payload.transactionType)
      .to.equal(constants.ORDERS.CLOSE_TYPE.FILL_OR_KILL_REFUND);

    // test fill or kill - matching prices - bid in
    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order9Id)
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.BID)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withPrice('2.5')
        .withQuantity('1000002')
        .withFillOrKill(true) //if this order is not filled in one go, close it and emit refund transaction, wont go into the orderbook if true
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    test.expect(orderBookSaved).to.eql(JSON.stringify(market.getOrderBook('BTC/XRP'), null, 2));

    [secondLastTransaction, lastTransaction] = market.eventQueueProvider.queued.slice(
      market.eventQueueProvider.queued.length - 2
    );

    test.expect(lastTransaction.eventType).to.equal(constants.EVENT_TYPE.ORDER_CLOSE);
    test
      .expect(lastTransaction.payload.reason)
      .to.equal(constants.ORDERS.CLOSE_TYPE.FILL_OR_KILL_REFUND);
    test.expect(lastTransaction.payload.order.orderId).to.equal(order9Id);
    test.expect(secondLastTransaction.eventType).to.equal(constants.EVENT_TYPE.TRANSACTION);
    test.expect(secondLastTransaction.payload.totalAmount).to.equal('1000002');
    test
      .expect(secondLastTransaction.payload.transactionType)
      .to.equal(constants.ORDERS.CLOSE_TYPE.FILL_OR_KILL_REFUND);

    // test getting orders for specific account
    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order10Id)
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.BID)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withPrice('2.5')
        .withQuantity('1000002')
        .withSpotAccountId(accountSpot1Id)
        .build()
    );
    test.expect(market.getOrdersByTradeAccountId('BTC/XRP', account1Id).length).to.equal(1);
    test.expect(market.getOrdersBySpotAccountId('BTC/XRP', accountSpot1Id).length).to.equal(1);
    test
      .expect(market.getOrdersByTradeAccountId('BTC/XRP', account1Id)[0].orderId)
      .to.equal(order10Id);
    test
      .expect(market.getOrdersBySpotAccountId('BTC/XRP', accountSpot1Id)[0].orderId)
      .to.equal(order10Id);
  });

  xit('limit orders with bid stop rules', async () => {
    const config = {};
    const eventQueueProviderImpl = require('../../__fixtures/providers/event-queue-provider');

    const market = Matchbox.Market(config, eventQueueProviderImpl);
    await setUpMarketScenario1(market);
    await market.start();

    const account1Id = test.uuid.v4();
    const account2Id = test.uuid.v4();
    const accountSpot1Id = test.uuid.v4();
    const accountSpot2Id = test.uuid.v4();

    const order1Id = test.uuid.v4();
    const order2Id = test.uuid.v4();
    const order3Id = test.uuid.v4();
    const order4Id = test.uuid.v4();
    const order5Id = test.uuid.v4();
    const order6Id = test.uuid.v4();
    const order7Id = test.uuid.v4();

    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order1Id)
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.BID)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withPrice('2.5')
        .withQuantity('100')
        .withSpotAccountId(accountSpot1Id)
        .build()
    );

    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order2Id)
        .withTradeAccountId(account2Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.OFFER)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withPrice('2.5')
        .withQuantity('400')
        .withSpotAccountId(accountSpot2Id)
        .build()
    );

    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order3Id)
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.BID)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withPrice('2.3')
        .withQuantity('100')
        .withSpotAccountId(accountSpot1Id)
        .withStopRule(
          Matchbox.StopRuleBuilder()
            .withLevel('2.5')
            .withPrice('0')
        ) //this will be refunded as a bad order as the stoprule is satisfied by the market price
        .build()
    );

    //push a bid in to the order book, current price is: 2.5
    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order4Id)
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.BID)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withPrice('2.3')
        .withQuantity('200')
        .withSpotAccountId(accountSpot1Id)
        .build()
    );

    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order5Id)
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.BID)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withPrice('2.3')
        .withQuantity('100')
        .withSpotAccountId(accountSpot1Id)
        .withStopRule(
          Matchbox.StopRuleBuilder()
            .withLevel('3')
            .withPrice('0')
        ) //if the price goes up to 3, we convert this to a market order as price is zero
        .build()
    );

    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order6Id)
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.BID)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withPrice('2.3')
        .withQuantity('100')
        .withSpotAccountId(accountSpot1Id)
        .withStopRule(
          Matchbox.StopRuleBuilder()
            .withLevel('3')
            .withPrice('3.01')
            .withFillOrKill(true)
        ) //if the price goes up to 3, we amend the price to 3.01 with fill or kill true
        .build()
    );

    //amend  the offer price to 3
    market.orderAmend(order2Id, { price: '3' });

    //throw in a matching bid - which should trigger our stop rules as the price has gone up to 3
    market.orderPush(
      Matchbox.OrderBuilder()
        .withOrderId(order7Id)
        .withTradeAccountId(account1Id)
        .withInstrument('BTC/XRP')
        .withDirection(constants.ORDERS.DIRECTION.BID)
        .withOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withPrice('3')
        .withQuantity('100')
        .withSpotAccountId(accountSpot1Id)
        .build()
    );

    let expectedQueueItems = mockData
      .getExpectedQueueItems4(
        account1Id,
        accountSpot1Id,
        account2Id,
        accountSpot2Id,
        order1Id,
        order2Id,
        order3Id,
        order4Id,
        order5Id,
        order6Id,
        order7Id
      )
      .map(removeTimestamps);

    //test.log(JSON.stringify(market.eventQueueProvider.queued, null, 2));
    test.expect(expectedQueueItems).to.eql(market.eventQueueProvider.queued.map(removeTimestamps));
  });

  function verifyOperations1(
    result1,
    result2,
    order1Id,
    account1Id,
    accountSpot1Id,
    order2Id,
    account2Id,
    accountSpot2Id
  ) {
    test
      .expect(
        [result1, result2].map(response => {
          delete response.timestamp;
          return response;
        })
      )
      .to.eql([
        {
          isTaker: false,
          fillOrKill: false,
          orderType: 1,
          orderId: order1Id,
          tradeAccountId: account1Id,
          instrument: 'BTC/XRP',
          direction: 0,
          quantity: '10',
          quantityRemaining: '10',
          price: '2',
          spotAccountId: accountSpot1Id
        },
        {
          isTaker: false,
          fillOrKill: false,
          orderType: 1,
          orderId: order2Id,
          tradeAccountId: account2Id,
          instrument: 'BTC/XRP',
          direction: 1,
          quantity: '5',
          quantityRemaining: '5',
          price: '2.5',
          spotAccountId: accountSpot2Id
        }
      ]);
  }

  async function setUpMarketScenario1(market) {
    market.addToken(
      Matchbox.TokenBuilder()
        .withCode('XRP')
        .withDescription('Ripple token')
        .withSmallestUnitName('drop')
        .withSmallestUnitScale('1000000')
        .build()
    );
    market.addToken(
      Matchbox.TokenBuilder()
        .withCode('BTC')
        .withDescription('Bitcoin token')
        .withSmallestUnitName('satoshi')
        .withSmallestUnitScale('100000000')
        .build()
    );
    market.addInstrument(
      Matchbox.InstrumentBuilder()
        .withAllowedOrderType(constants.ORDERS.ORDER_TYPE.LIMIT)
        .withAllowedOrderType(constants.ORDERS.ORDER_TYPE.MARKET)
        .withAllowedOrderType(constants.ORDERS.ORDER_TYPE.MID_POINT_PEGGED)
        .withBaseToken(
          Matchbox.PairItemBuilder(market.getToken('BTC'))
            .withMinFee('0.00001')
            .withMakerFeePercentage('0.1')
            .withTakerFeePercentage('0.2')
        )
        .withQuoteToken(
          Matchbox.PairItemBuilder(market.getToken('XRP'))
            .withMinFee('0.0001')
            .withMakerFeePercentage('0.1')
            .withTakerFeePercentage('0.2')
        )
        .build()
    );
    await market.activateInstrument('BTC/XRP');
    test.expect(market.getTokens()).to.eql([
      {
        code: 'XRP',
        description: 'Ripple token',
        smallestUnitName: 'drop',
        smallestUnitScale: '1000000'
      },
      {
        code: 'BTC',
        description: 'Bitcoin token',
        smallestUnitName: 'satoshi',
        smallestUnitScale: '100000000'
      }
    ]);
    test.expect(market.getInstrumentConfigs()).to.eql([
      {
        active: true,
        pair: 'BTC/XRP',
        allowedOrderType: [1, 0, 2],
        baseToken: {
          tokenCode: 'BTC',
          minFee: '0.00001',
          makerFeePercentage: '0.1',
          takerFeePercentage: '0.2',
          smallestUnitName: 'satoshi',
          smallestUnitScale: '100000000',
          scale: 8
        },
        quoteToken: {
          tokenCode: 'XRP',
          minFee: '0.0001',
          makerFeePercentage: '0.1',
          takerFeePercentage: '0.2',
          smallestUnitName: 'drop',
          smallestUnitScale: '1000000',
          scale: 6
        }
      }
    ]);
  }

  function removeTimestamps(item) {
    if (item.payload.order) delete item.payload.order.timestamp;
    if (item.payload.timestamp) delete item.payload.timestamp;
    return item;
  }
});
