const test = require('../../../../__fixtures/test-helper').create();
const utils = require('../../../../../lib/utils/base-utils').create();
describe(test.name(__filename), function() {
  this.timeout(360000);
  it('tests various calculations', async () => {
    const overlap = utils.orderCalculations.calculateOverlap('500', '1500', '2', 8);
    test.expect(overlap.baseQuantity).to.equal('500');
    test.expect(overlap.quoteQuantity.result()).to.equal('1000');
  });
});
