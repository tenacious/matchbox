const test = require('../../../../__fixtures/test-helper').create();
const utils = require('../../../../../lib/utils/base-utils').create();
describe(test.name(__filename), function() {
  this.timeout(360000);
  it('tests various calculations', async () => {
    test
      .expect(
        utils.numbers
          .chain('0.1', 1)
          .add('0.2')
          .result()
      )
      .to.equal('0.3');
    test
      .expect(
        utils.numbers
          .chain('0.12342526')
          .truncate(6)
          .result()
      )
      .to.equal('0.123425');

    test
      .expect(
        utils.numbers
          .chain('0.1234256')
          .truncate(6)
          .result()
      )
      .to.equal('0.123425');
  });
});
