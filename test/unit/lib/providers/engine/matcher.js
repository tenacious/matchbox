const test = require('../../../../__fixtures/test-helper').create();
const Matcher = require('../../../../../lib/providers/engine/matcher');
const FillBuilder = require('../../../../../lib/builders/fill-builder.js');
describe(test.name(__filename), function() {
  this.timeout(360000);
  it('tests the FillBuilder', async () => {
    const fill = FillBuilder.create(mockInstrumentConfig1())
      .withBidOrderId('9eb06085-7292-4aa9-9a07-b33a5855085e')
      .withBidToOffer('499000.00')
      .withBidToFee('1000.00')
      .withBidRefund('0.00')
      .withOfferOrderId('b59e8f91-0df4-4024-823f-7e373c4abcf7')
      .withOfferToBid('0.99900000')
      .withOfferRefund('0.00000000')
      .withOfferToFee('0.00100000')
      .build();
    test.expect(fill).to.eql({
      timestamp: fill.timestamp,
      bidOrderId: '9eb06085-7292-4aa9-9a07-b33a5855085e',
      bidToOffer: '499000.00',
      bidToFee: '1000.00',
      bidRefund: '0.00',
      offerOrderId: 'b59e8f91-0df4-4024-823f-7e373c4abcf7',
      offerToBid: '0.99900000',
      offerToFee: '0.00100000',
      offerRefund: '0.00000000',
      totalBidFilled: '500000',
      totalOfferFilled: '1'
    });
  });
  xit('matches bids and offers', async () => {
    const mockInstrumentConfig = mockInstrumentConfig1();
    const matcher = Matcher.create(mockInstrumentConfig);
    // 2 limit orders - perfectly matched, bid maker
    const matchResult = matcher.match({
      bids: { '1': ['500000', '500000', '9eb06085-7292-4aa9-9a07-b33a5855085e', 1623853330326] },
      offers: { '1': ['500000', '1', 'b59e8f91-0df4-4024-823f-7e373c4abcf7', 1623853354249] },
      bidCount: 1,
      offerCount: 1
    });
    test.expect(matchResult).to.eql(
      FillBuilder.create(mockInstrumentConfig)
        .withTimestamp(matchResult.timestamp)
        .withBidOrderId('9eb06085-7292-4aa9-9a07-b33a5855085e')
        .withBidPrice('500000')
        .withBidQuantity('500000')
        .withBidRefund('0')
        .withBidRemainder('0')
        .withBidToOffer('499000')
        .withBidToFee('1000')
        .withOfferOrderId('b59e8f91-0df4-4024-823f-7e373c4abcf7')
        .withOfferPrice('500000')
        .withOfferQuantity('1')
        .withOfferRefund('0')
        .withOfferRemainder('0')
        .withOfferToBid('0.999')
        .withOfferToFee('0.001')
        .withFillPrice('500000')
        .build()
    );
    // 2 limit orders - perfectly matched, offer maker
    const matchResult1 = matcher.match({
      bids: { '1': ['500000', '500000', '9eb06085-7292-4aa9-9a07-b33a5855085e', 1623853354249] },
      offers: { '1': ['500000', '1', 'b59e8f91-0df4-4024-823f-7e373c4abcf7', 1623853330326] },
      bidCount: 1,
      offerCount: 1
    });
    test.expect(matchResult1).to.eql(
      FillBuilder.create(mockInstrumentConfig)
        .withTimestamp(matchResult1.timestamp)
        .withBidOrderId('9eb06085-7292-4aa9-9a07-b33a5855085e')
        .withBidPrice('500000')
        .withBidQuantity('500000')
        .withBidRefund('0')
        .withBidRemainder('0')
        .withBidToOffer('499500')
        .withBidToFee('500')
        .withOfferOrderId('b59e8f91-0df4-4024-823f-7e373c4abcf7')
        .withOfferPrice('500000')
        .withOfferQuantity('1')
        .withOfferRefund('0')
        .withOfferRemainder('0')
        .withOfferToBid('0.998')
        .withOfferToFee('0.002')
        .withFillPrice('500000')
        .build()
    );
  });
  function mockInstrumentConfig1() {
    return {
      allowedOrderType: [0, 1, 2, 3], //all order types
      baseToken: {
        tokenCode: 'BTC',
        minFee: '0.00001',
        makerFeePercentage: '0.1',
        takerFeePercentage: '0.2',
        smallestUnitName: 'satoshi',
        smallestUnitScale: '100000000',
        precision: 8
      },
      quoteToken: {
        tokenCode: 'ZAR',
        minFee: '0.01',
        makerFeePercentage: '0.1',
        takerFeePercentage: '0.2',
        smallestUnitName: 'cent',
        smallestUnitScale: '100',
        precision: 2
      },
      pair: 'BTC/ZAR'
    };
  }
});
