const test = require('../../../../__fixtures/test-helper').create();
const OrderBook = require('../../../../../lib/providers/instrument/order-book');
const constants = require('../../../../../lib/constants');
describe(test.name(__filename), function() {
  this.timeout(360000);
  it('order book unit tests', async () => {
    const orderBook = OrderBook.create(mockInstrumentConfig1());
    orderBook.add(
      constants.ORDERS.DIRECTION.BID,
      constants.ORDERS.ORDER_TYPE.LIMIT,
      '0.49',
      '1000'
    );
    orderBook.add(constants.ORDERS.DIRECTION.OFFER, constants.ORDERS.ORDER_TYPE.LIMIT, '0.49', '1');
    test
      .expect(orderBook.generate())
      .to.eql([[['0.49', '1', '0.49']], [['0.49', '2040.81632653', '1000']]]);

    orderBook.add(
      constants.ORDERS.DIRECTION.BID,
      constants.ORDERS.ORDER_TYPE.LIMIT,
      '0.50',
      '1000'
    );
    orderBook.add(constants.ORDERS.DIRECTION.OFFER, constants.ORDERS.ORDER_TYPE.LIMIT, '0.50', '1');
    test.expect(orderBook.generate()).to.eql([
      [
        ['0.49', '1', '0.49'],
        ['0.50', '1', '0.5']
      ],
      [
        ['0.50', '2000', '1000'],
        ['0.49', '2040.81632653', '1000']
      ]
    ]);
    orderBook.remove(
      constants.ORDERS.DIRECTION.OFFER,
      constants.ORDERS.ORDER_TYPE.LIMIT,
      '0.50',
      '1'
    );
    test.expect(orderBook.generate()).to.eql([
      [['0.49', '1', '0.49']],
      [
        ['0.50', '2000', '1000'],
        ['0.49', '2040.81632653', '1000']
      ]
    ]);
    orderBook.remove(
      constants.ORDERS.DIRECTION.BID,
      constants.ORDERS.ORDER_TYPE.LIMIT,
      '0.50',
      '250'
    );
    test.expect(orderBook.generate()).to.eql([
      [['0.49', '1', '0.49']],
      [
        ['0.50', '1500', '750'],
        ['0.49', '2040.81632653', '1000']
      ]
    ]);
    orderBook.remove(
      constants.ORDERS.DIRECTION.OFFER,
      constants.ORDERS.ORDER_TYPE.LIMIT,
      '0.49',
      '0.50'
    );
    test.expect(orderBook.generate()).to.eql([
      [['0.49', '0.5', '0.245']],
      [
        ['0.50', '1500', '750'],
        ['0.49', '2040.81632653', '1000']
      ]
    ]);
  });

  function mockInstrumentConfig1() {
    return {
      allowedOrderType: [0, 1, 2, 3], //all order types
      baseToken: {
        tokenCode: 'BTC',
        minFee: '0.00001',
        makerFeePercentage: '0.1',
        takerFeePercentage: '0.2',
        smallestUnitName: 'satoshi',
        smallestUnitScale: '100000000',
        scale: 8
      },
      quoteToken: {
        tokenCode: 'XRP',
        minFee: '0.000001',
        makerFeePercentage: '0.1',
        takerFeePercentage: '0.2',
        smallestUnitName: 'drop',
        smallestUnitScale: '1000000',
        scale: 6
      },
      pair: 'BTC/ZAR'
    };
  }
});
