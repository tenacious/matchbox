const OrderListSorted = require('../../../lib/providers/engine/sorted-order-list');
module.exports = class OrderListBenchmarkHelper {
    static inserts (
        test,
        insertCount,
        logModulus,
        descending,
        durationLimit,
        priceVarience,
        bucketCount
      ) {
        const started = Date.now();
        priceVarience = priceVarience || 100;
        const sortedOrderList = OrderListSorted.create(descending, null, bucketCount || 10);
        for (var i = 0; i < insertCount; i++) {
          const random = Math.floor(Math.random() * priceVarience) + 1;
          // eslint-disable-next-line no-console
          if (i >= logModulus && i % logModulus === 0) {
            const best = sortedOrderList.head();
            // eslint-disable-next-line no-console
            console.log(`did ${i} inserts, best at the moment is: ${best[0]}`);
            sortedOrderList.remove(best);
          }
          sortedOrderList.insert([random, random + 100, i]);
        }
        const duration = Date.now() - started;
        // eslint-disable-next-line no-console
        console.log(
          `duration: ${duration}ms - average speed of ${insertCount / (duration / 1000)} per sec`
        );
        test.expect(duration <= durationLimit).to.equal(true);
      }
}