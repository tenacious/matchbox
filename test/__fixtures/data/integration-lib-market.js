module.exports = class TestData {
    constructor() {

    }
    static create() {
        return new TestData();
    }
    getExpectedQueueItems1(account2Id, accountSpot2Id, order3Id, order5Id) {
        return [
          {
            eventType: 5,
            payload: {
              amount: '10',
              sourceAccountId: account2Id,
              destinationAccountId: accountSpot2Id,
              operationType: 1,
              transactionType: 0,
              sourceOrderId: order3Id,
              tokenCode: 'BTC',
              totalAmount: '10'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 5,
            payload: {
              amount: '10',
              sourceAccountId: account2Id,
              destinationAccountId: accountSpot2Id,
              operationType: 1,
              transactionType: 0,
              sourceOrderId: order5Id,
              tokenCode: 'XRP',
              totalAmount: '10'
            },
            pair: 'BTC/XRP'
          }
        ];
      }
    
      getExpectedQueueItems2(
        account1Id,
        accountSpot1Id,
        account2Id,
        accountSpot2Id,
        order1Id,
        order2Id,
        order3Id,
        order5Id
      ) {
        return [
          {
            eventType: 5,
            payload: {
              amount: '10',
              sourceAccountId: account2Id,
              destinationAccountId: accountSpot2Id,
              operationType: 1,
              transactionType: 0,
              sourceOrderId: order3Id,
              tokenCode: 'BTC',
              totalAmount: '10'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 5,
            payload: {
              amount: '10',
              sourceAccountId: account2Id,
              destinationAccountId: accountSpot2Id,
              operationType: 1,
              transactionType: 0,
              sourceOrderId: order5Id,
              tokenCode: 'XRP',
              totalAmount: '10'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 5,
            payload: {
              amount: '0.000001',
              sourceAccountId: account1Id,
              destinationAccountId: accountSpot1Id,
              operationType: 6,
              transactionType: 2,
              sourceOrderId: order1Id,
              tokenCode: 'XRP',
              orderPrice: '2.3',
              fillPrice: '2.3',
              totalAmount: '0.000001'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 5,
            payload: {
              amount: '9.99',
              sourceAccountId: account1Id,
              destinationAccountId: accountSpot2Id,
              operationType: 6,
              transactionType: 3,
              sourceOrderId: order1Id,
              destinationOrderId: order2Id,
              fee: '0.009999',
              tokenCode: 'XRP',
              orderPrice: '2.3',
              fillPrice: '2.3',
              totalAmount: '9.999999'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 5,
            payload: {
              amount: '4.34347826',
              sourceAccountId: account2Id,
              destinationAccountId: accountSpot1Id,
              operationType: 6,
              transactionType: 3,
              sourceOrderId: order2Id,
              destinationOrderId: order1Id,
              fee: '0.00434782',
              tokenCode: 'BTC',
              orderPrice: '2.3',
              fillPrice: '2.3',
              totalAmount: '4.34782608'
            },
            pair: 'BTC/XRP'
          }
        ];
      }
    
      getExpectedQueueItems3(
        account1Id,
        accountSpot1Id,
        account2Id,
        accountSpot2Id,
        order1Id,
        order2Id,
        order3Id,
        order4Id,
        order5Id
      ) {
        return [
          {
            eventType: 5,
            payload: {
              amount: '10',
              sourceAccountId: account2Id,
              destinationAccountId: accountSpot2Id,
              operationType: 1,
              transactionType: 0,
              sourceOrderId: order3Id,
              tokenCode: 'BTC',
              totalAmount: '10'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 1,
            payload: {
              order: {
                fillOrKill: false,
                orderType: 1,
                timestamp: 1627636083099,
                orderId: order3Id,
                tradeAccountId: account2Id,
                instrument: 'BTC/XRP',
                direction: 1,
                quantityRemaining: '10',
                quantity: '10',
                price: '2.4',
                spotAccountId: accountSpot2Id,
                isTaker: false
              },
              reason: 0
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 5,
            payload: {
              amount: '10',
              sourceAccountId: account2Id,
              destinationAccountId: accountSpot2Id,
              operationType: 1,
              transactionType: 0,
              sourceOrderId: order5Id,
              tokenCode: 'XRP',
              totalAmount: '10'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 1,
            payload: {
              order: {
                fillOrKill: false,
                orderType: 1,
                timestamp: 1627636083101,
                orderId: order5Id,
                tradeAccountId: account2Id,
                instrument: 'BTC/XRP',
                direction: 0,
                quantityRemaining: '10',
                quantity: '10',
                price: '2.2',
                spotAccountId: accountSpot2Id,
                isTaker: false
              },
              reason: 0
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 5,
            payload: {
              amount: '0.000001',
              sourceAccountId: account1Id,
              destinationAccountId: accountSpot1Id,
              operationType: 6,
              transactionType: 2,
              sourceOrderId: order1Id,
              tokenCode: 'XRP',
              orderPrice: '2.3',
              fillPrice: '2.3',
              totalAmount: '0.000001'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 5,
            payload: {
              amount: '9.99',
              sourceAccountId: account1Id,
              destinationAccountId: accountSpot2Id,
              operationType: 6,
              transactionType: 3,
              sourceOrderId: order1Id,
              destinationOrderId: order2Id,
              fee: '0.009999',
              tokenCode: 'XRP',
              orderPrice: '2.3',
              fillPrice: '2.3',
              totalAmount: '9.999999'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 5,
            payload: {
              amount: '4.34347826',
              sourceAccountId: account2Id,
              destinationAccountId: accountSpot1Id,
              operationType: 6,
              transactionType: 3,
              sourceOrderId: order2Id,
              destinationOrderId: order1Id,
              fee: '0.00434782',
              tokenCode: 'BTC',
              orderPrice: '2.3',
              fillPrice: '2.3',
              totalAmount: '4.34782608'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 1,
            payload: {
              order: {
                fillOrKill: false,
                orderType: 1,
                orderId: order1Id,
                tradeAccountId: account1Id,
                instrument: 'BTC/XRP',
                direction: 0,
                quantityRemaining: '0',
                quantity: '10',
                price: '2.3',
                spotAccountId: accountSpot1Id,
                isTaker: false
              },
              reason: 3
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 0,
            payload: {
              fillOrKill: false,
              orderType: 1,
              orderId: order2Id,
              tradeAccountId: account2Id,
              instrument: 'BTC/XRP',
              direction: 1,
              quantityRemaining: '0',
              quantity: '5',
              price: '2.2',
              spotAccountId: accountSpot2Id,
              isTaker: true
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 2,
            payload: {
              timestamp: 1627636083103,
              bidOrderId: order1Id,
              bidQuantity: '10',
              bidToOffer: '9.99',
              bidToFee: '0.009999',
              offerOrderId: order2Id,
              offerQuantity: '5',
              offerToBid: '4.34347826',
              offerToFee: '0.00434782',
              bidRefund: '0.000001',
              offerRefund: '0',
              bidRemainder: '0',
              offerRemainder: '0.65217392',
              bidPrice: '2.3',
              offerPrice: '2.3',
              fillPrice: '2.3',
              totalBidFilled: '10',
              totalOfferFilled: '4.34782608'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 5,
            payload: {
              amount: '1.433348',
              sourceAccountId: account2Id,
              destinationAccountId: accountSpot2Id,
              operationType: 6,
              transactionType: 3,
              sourceOrderId: order4Id,
              destinationOrderId: order2Id,
              fee: '0.001434',
              tokenCode: 'XRP',
              orderPrice: '2.2',
              fillPrice: '2.2',
              totalAmount: '1.434782'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 5,
            payload: {
              amount: '0.65152175',
              sourceAccountId: account2Id,
              destinationAccountId: accountSpot2Id,
              operationType: 6,
              transactionType: 3,
              sourceOrderId: order2Id,
              destinationOrderId: order4Id,
              fee: '0.00065217',
              tokenCode: 'BTC',
              orderPrice: '2.2',
              fillPrice: '2.2',
              totalAmount: '0.65217392'
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 0,
            payload: {
              fillOrKill: false,
              orderType: 1,
              timestamp: 1627636083100,
              orderId: order4Id,
              tradeAccountId: account2Id,
              instrument: 'BTC/XRP',
              direction: 0,
              quantityRemaining: '8.565218',
              quantity: '10',
              price: '2.2',
              spotAccountId: accountSpot2Id,
              isTaker: false
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 1,
            payload: {
              order: {
                fillOrKill: false,
                orderType: 1,
                orderId: order2Id,
                tradeAccountId: account2Id,
                instrument: 'BTC/XRP',
                direction: 1,
                quantityRemaining: '0',
                quantity: '5',
                price: '2.2',
                spotAccountId: accountSpot2Id,
                isTaker: true
              },
              reason: 3
            },
            pair: 'BTC/XRP'
          },
          {
            eventType: 2,
            payload: {
              timestamp: 1627636083106,
              bidOrderId: order4Id,
              bidQuantity: '10',
              bidToOffer: '1.433348',
              bidToFee: '0.001434',
              offerOrderId: order2Id,
              offerQuantity: '5',
              offerToBid: '0.65152175',
              offerToFee: '0.00065217',
              bidRefund: '0',
              offerRefund: '0',
              bidRemainder: '8.565218',
              offerRemainder: '0',
              bidPrice: '2.2',
              offerPrice: '2.2',
              fillPrice: '2.2',
              totalBidFilled: '1.434782',
              totalOfferFilled: '0.65217392'
            },
            pair: 'BTC/XRP'
          }
        ];
      }
    
      getExpectedQueueItems4(
        account1Id,
        accountSpot1Id,
        account2Id,
        accountSpot2Id,
        order1Id,
        order2Id,
        order3Id,
        order4Id,
        order5Id,
        order6Id,
        order7Id
      ) {
        return [];
      }
}