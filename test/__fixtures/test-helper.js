module.exports = class TestHelper {
    constructor() {
        this.assert = require('assert');
        this.expect = require('chai').expect;
        this.delay = require('await-delay');
        this.path = require('path');
        this.fs = require('fs');
        this.mustache = require('mustache');
        this.uuid = require('uuid');
        this.utils = require('../../lib/utils/base-utils').create();
    }
    static create() {
        return new TestHelper();
    }
    name (filename, depth = 4) {
        const segments = filename.split(this.path.sep);
        if (segments.length < depth) return segments.join(' / ');
        return segments.slice(segments.length - depth).join(' / ');
    }
    log(msg, data) {
        if (typeof msg === "object")
            console.log(JSON.stringify(msg, null, 2));
        else console.log(msg);
        if (data) console.log(JSON.stringify(data, null, 2));
    }
    logJSON(msg) {
        console.log(JSON.stringify(msg, null, 2));
    }
}