const constants = require('../../../lib/constants');
const utils = require('../../../lib/utils/base-utils').create();
module.exports = class OrderValidator {
    static verifyOrderListAgainstOrderBook(test, market) {
        const instrument = market.getInstrument('BTC/XRP');
        OrderValidator.checkOrderListAgainstOrderBook(
          test,
          instrument.engine.orderLists,
          constants.ORDERS.DIRECTION.OFFER,
          constants.ORDERS.ORDER_TYPE.LIMIT,
          market.getOrderBook('BTC/XRP')
        );
        OrderValidator.checkOrderListAgainstOrderBook(
          test,
          instrument.engine.orderLists,
          constants.ORDERS.DIRECTION.BID,
          constants.ORDERS.ORDER_TYPE.LIMIT,
          market.getOrderBook('BTC/XRP')
        );
      }
    static checkOrderListAgainstOrderBook(test, orderLists, direction, orderType, orderBook){
       if (!orderLists[direction]) {
        if (direction === constants.ORDERS.DIRECTION.OFFER) test.expect(orderBook[0]).to.eql([]);
        else test.expect(orderBook[1]).to.eql([]);
        return;
       };

       const orderList = orderLists[direction][orderType];
       const orderListHeap = Array.from(orderList.heap.keys()).reduce((arrOrders, orderKey) => {
        arrOrders.push(orderList.heap.get(orderKey).slice(0, 2));
        return arrOrders;
       }, []);

       const orderListStack = Object.keys(orderList.stack).reduce((arrOrders, stackKey) => {
        arrOrders.push(orderList.stack[stackKey].slice(0, 2));
        return arrOrders;
       }, []);

       orderListStack.sort((stackItemA, stackItemB) => {
            if (direction === constants.ORDERS.DIRECTION.OFFER)
                return parseFloat(utils.numbers.chain(stackItemA[0], 8).subtract(stackItemB[0]).result());
            else 
                return parseFloat(utils.numbers.chain(stackItemB[0], 8).subtract(stackItemA[0]).result());
        });

       orderListHeap.sort((heapItemA, heapItemB) => {
            if (direction === constants.ORDERS.DIRECTION.OFFER)
                return parseFloat(utils.numbers.chain(heapItemA[0], 8).subtract(heapItemB[0]).result());
            else 
                return parseFloat(utils.numbers.chain(heapItemB[0], 8).subtract(heapItemA[0]).result());
       });

       test.expect(orderListHeap).to.eql(orderListStack);
       
       const orderBookDirection = (direction === constants.ORDERS.DIRECTION.OFFER ? orderBook[0] : orderBook[1]).map(orderBookDirectionItem => {
           return [orderBookDirectionItem[0], direction === constants.ORDERS.DIRECTION.OFFER ? orderBookDirectionItem[1] : orderBookDirectionItem[2]]
       });

       const orderListCompareObject = OrderValidator.constructOrderBookObject(orderListHeap);
       const orderBookDirectionObject = OrderValidator.constructOrderBookObject(orderBookDirection);

       test.expect(orderListCompareObject).to.eql(orderBookDirectionObject);
    }
    static constructOrderBookObject(orderList) {
        return orderList.reduce((obj, orderListItem) => {
            const orderListItemKey = parseFloat(orderListItem[0]).toString();
            if (!obj[orderListItemKey]) obj[orderListItemKey] = '0';
            obj[orderListItemKey] = utils.numbers.chain(obj[orderListItemKey], 8).add(orderListItem[1]).result();
            return obj;
        }, {});
    }
}