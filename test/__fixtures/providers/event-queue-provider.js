const utils = require('../../../lib/utils/base-utils');
const constants = require('../../../lib/constants');
module.exports = class EventQueueProviderTest extends require('../../../lib/providers/queue/queue-base-provider') {
    constructor() {
        super();
        this.accumulated = {};
        this.queued = [];
        this.accumulator = require('../helpers/accumulator').create();
    }
    static create() {
        return new EventQueueProviderTest();
    }
    async startInternal() {
        
    }
    async pushInternal(msg) {
        if (msg.eventType === constants.EVENT_TYPE.TRANSACTION) {
            if (!this.accumulated[msg.payload.transactionType])
                this.accumulated[msg.payload.transactionType] = 0;
            this.accumulated[msg.payload.transactionType]++;
            this.accumulator.add(msg.payload.tokenCode, msg.payload.amount, msg.payload.fee);
        }
        this.queued.push(msg);
    }
}