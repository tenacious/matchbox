module.exports = class PersistenceActiveProviderTest extends require('../../../lib/providers/persistence/persistence-base-provider') {
    constructor() {
        super();
        this.data = {};
    }
    static create() {
        return new PersistenceActiveProviderTest();
    }
    async startInternal() {
        
    }
    async stopInternal() {
        
    }
    async setInternal(context, entityId, entity) {
        this.data[`${context}/${entityId}`] = entity;
    }
    async getInternal(context, entityId) {
        return this.data[`${context}/${entityId}`];
    }
    async removeInternal(context, entityId) {
        delete this.data[`${context}/${entityId}`];
    }
}