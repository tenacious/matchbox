const utils = require('../../../lib/utils/base-utils');
module.exports = class TransactionQueueProviderTestBenchmark extends require('../../../lib/providers/queue/queue-base-provider') {
    constructor() {
        super();
        this.queued = {};
        this.accumulator = require('../helpers/accumulator').create();
    }
    static create() {
        return new TransactionQueueProviderTestBenchmark();
    }
    async startInternal() {
        
    }
    async pushInternal(msg) {
        if (!this.queued[msg.transactionType])
            this.queued[msg.transactionType] = 0;
        this.queued[msg.transactionType]++;
        this.accumulator.add(msg.tokenCode, msg.amount, msg.fee);
    }
}