const utils = require('../../../lib/utils/base-utils');
const constants = require('../../../lib/constants');
module.exports = class EventQueueProviderTest extends require('../../../lib/providers/queue/queue-base-provider') {
    constructor() {
        super();
        this.queued = 0;
    }
    static create() {
        return new EventQueueProviderTest();
    }
    async startInternal() {
        
    }
    async pushInternal(msg) {
        if (msg.eventType === constants.EVENT_TYPE.TRANSACTION)
            this.queued++;
    }
}