module.exports = class TransactionQueueProviderTest extends require('../../../lib/providers/queue/queue-base-provider') {
    constructor() {
        super();
        this.queued = [];
    }
    static create() {
        return new TransactionQueueProviderTest();
    }
    async startInternal() {
        
    }
    async pushInternal(msg) {
        this.queued.push(msg);
    }
}