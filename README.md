# matchbox-js

matchbox-js is a lightweight javascript matching engine and order book, orders are processed synchronously and the resulting events (fills, cancellations and refunds) are outputted to an injected handler. Although the orders and orderbook are held in memory, matchbox does not persist anything - event sourcing, validation and persistence would all need to be managed by the service using matchbox to match orders. 

Matchbox is benchmarked for processing about 20000 match operations per second on a standard dev machine (i5 processor 2.3 GHz), this means if you are wanting hardcore-concurrency via an lmax disruptor and sub-microsecond trades, a better bet would be something multi-threaded like [exchange-core](https://github.com/mzheravin/exchange-core) (which also takes care of persistence and balance validation)

matchbox could ideally be used by slower exchanges, or by people who want to test javascript-based bots without spinning up a whole exchange api.

## order types
- LIMIT - are set to match at a specific price
- MARKET - will match whatever is in the order book, if they cannot be filled in one go the remainder is refunded immediately
- MID_POINT_PEGGED - dynamic orders with the price set at the exact centre of the current market spread, so if your lowest limit offer is 3 and your highest limit bid is 2, your midpoint pegged orders would be matching at (2 + 3) / 2 = 2.5

## assumptions, limits and peculiarities

- bid orders quantity and token are in the quote currency, offer orders quantity and token are in the base currency, so if the instrument is XRP/USD and the price is 1/2, to bid for 1 XRP, you would include 2 as your quantity *as you are bidding in USD* . Your offer would be in XRP, so the offer quantity would be 1 to create a perfect match.

- matchbox is not a wallet service, and so autistically assumes that the source of the order is good-for-the-money, orders must have a trade account id and a spot (current) account id, refunds and trade amounts move to the spot account, from the trade account - matchbox outputs transactions with the correct amounts and destination account ids as matches etc. 

- matchbox assumes that when an order is made, funds are reserved from the spot account to the trade account, and the resulting transaction id is passed to the matching engine possibly as the order id. Matchbox then outputs TRANSACTION events that specify which trade accounts need to be empties into which spot accounts.

- stop limits and losses are in the order builders, but are not ready yet, watch that space...

## getting started
### install and run some test scripts:

```bash
git clone https://gitlab.com/tenacious/matchbox.git && cd matchbox && npm i
npm run test #integration and unit tests
node test/benchmark/lib/market-no-mocha #benchmark tests
```

### check out the demo code for working with matchbox as a module [here](https://gitlab.com/tenacious/matchbox-demo)